<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Admincontroller extends CI_controller {

    var $headervar;
    var $menuvar;
    var $footervar;
    var $titulo,$unidade,$campo_1,$campo_2,$campo_3;

    function __construct() {
        parent::__construct();
        
        if(!$this->session->userdata('logged_in'))
            redirect('painel/home/index');

        if($this->session->flashdata('mostrarerro') === true){
            $this->headervar['mostrarerro'] = true;
            $this->menuvar['mostrarerro_mensagem'] = $this->session->flashdata('mostrarerro_mensagem');
        }else{
            $this->headervar['mostrarerro'] = false;
            $this->menuvar['mostrarerro_mensagem'] = '';
        }

        if($this->session->flashdata('mostrarsucesso') === true){
            $this->headervar['mostrarsucesso'] = true;
            $this->menuvar['mostrarsucesso_mensagem'] = $this->session->flashdata('mostrarsucesso_mensagem');
        }else{
            $this->headervar['mostrarsucesso'] = false;
            $this->menuvar['mostrarsucesso_mensagem'] = '';
        }
    }

    function index(){
        $data['registros'] = $this->model->pegarTodos();

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $data['campo_1'] = $this->campo_1;
        $data['campo_2'] = $this->campo_2;
        $data['campo_3'] = $this->campo_3;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['registro'] = FALSE;
        }

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }

    function inserir(){
        if($this->model->inserir()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' inserido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index', 'refresh');
    }

    function alterar($id){
        if($this->model->alterar($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' alterado com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index', 'refresh');
    }

    function excluir($id){
        if($this->model->excluir($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' excluido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index', 'refresh');
    }

    function imagens($id_parent, $id_imagem =  FALSE){
        if($id_imagem){
            $data['parent'] = $this->model->pegarPorId($id_parent);
            $data['imagens'] = $this->model->imagens($id_parent);
            $data['registro'] = $this->model->imagens($id_parent, $id_imagem);
            if(!$data['album'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['parent'] = $this->model->pegarPorId($id_parent);
            $data['imagens'] = $this->model->imagens($id_parent);
            $data['registro'] = FALSE;
        }

        if(isset($data['parent']->titulo))
            $titulo_atual = $data['parent']->titulo;
        elseif(isset($data['parent']->nome))
            $titulo_atual = $data['parent']->nome;
        else
            $titulo_atual = $this->titulo;

        $data['campo_1'] = "Imagem";
        $data['titulo'] = 'Galeria de imagens de : '.$titulo_atual;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/imagens', $data);
    }

    function inserirImagem(){
        if($this->model->inserirImagem()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem inserida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$this->input->post('id_parent'), 'refresh');
    }

    function editarImagem($id_imagem){
        if($this->model->editarImagem($id_imagem)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem alterada com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$this->input->post('id_parent'), 'refresh');
    }

    function excluirImagem($id_imagem, $id_album){
        if($this->model->excluirImagem($id_imagem)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem excluida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$id_album, 'refresh');
    }


    function _output($output){
        echo $this->load->view('painel/common/header', $this->headervar, TRUE).
             $this->load->view('painel/common/menu', $this->menuvar, TRUE).
             $output.
             $this->load->view('painel/common/footer', $this->footervar, TRUE);
    }
}
?>
