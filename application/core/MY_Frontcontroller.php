<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class MY_Frontcontroller extends CI_controller {

    var $headervar;
    var $footervar;
    var $menuvar;
    var $hasLayout;

    function __construct($css = '', $js = '') {
        parent::__construct();
        
        $this->headervar['load_css'] = $css;
        $this->headervar['load_js'] = $js;
        //$this->output->enable_profiler(TRUE);
        $this->menuvar['submenu_boutique'] = $this->db->get_where('categorias', array('id_secoes' => 11))->result();
        $this->menuvar['submenu_alimentacao'] = $this->db->get_where('categorias', array('id_secoes' => 12))->result();
        $this->menuvar['submenu_higiene'] = $this->db->get_where('categorias', array('id_secoes' => 13))->result();
        $this->menuvar['submenu_farmacia'] = $this->db->get_where('categorias', array('id_secoes' => 14))->result();
        $this->menuvar['slides'] = $this->db->order_by('ordem', 'asc')->get('slides')->result();
        $this->hasLayout = TRUE;
    }
    
    function _output($output){

        if($this->hasLayout):

            echo $this->load->view('common/header', $this->headervar, TRUE).
                 $this->load->view('common/menu', $this->menuvar, TRUE).
                 $output.
                 $this->load->view('common/footer', $this->footervar, TRUE);

        else:
            echo $output;
        endif;
    }

}
?>