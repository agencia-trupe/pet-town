<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Meupet_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'meupet';

		$this->dados = array('publicar');
		$this->dados_tratados = array();
	}

	function pegarTodos(){
		return $this->db->order_by('publicar', 'asc')->order_by('ordem', 'asc')->get($this->tabela)->result();
	}

	function pegarAprovados($porpagina = 5){
		return $this->db->order_by('ordem', 'asc')->get_where($this->tabela, array('publicar' => 1), $porpagina, 0)->result();	
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/galerias/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	        		->load($original['dir'].$filename)
	                ->resize(300, 800)
	                ->save($original['dir'].$filename, TRUE)
	                ->resize_crop(180,180)
	                ->save($original['dir'].'thumbs/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	

}