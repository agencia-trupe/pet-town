<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'produtos';
		$this->tabela_imagens = 'variacoes';

		$this->dados = array('titulo', 'slug', 'descricao', 'publicar');
		$this->dados_tratados = array(
			'slug' => url_title($this->input->post('titulo'), '_', TRUE)
		);
		
	}

	function pegarTodos($num_resultados = FALSE, $per_page = 30, $pagina = 0){
		return ($num_resultados) ?  $this->db->get($this->tabela)->num_rows() : $this->db->get($this->tabela, $per_page, $pagina)->result();
	}	

	function busca($termo, $secao = false){

$query = <<<QRY
SELECT DISTINCT produtos.* FROM produtos
LEFT JOIN rel_categorias_produtos ON produtos.id = rel_categorias_produtos.id_produtos
LEFT JOIN categorias ON rel_categorias_produtos.id_categorias = categorias.id
LEFT JOIN secoes ON categorias.id_secoes = secoes.id
LEFT JOIN rel_animais_produtos ON rel_animais_produtos.id_produtos = produtos.id
LEFT JOIN animais ON rel_animais_produtos.id_animais = animais.id
LEFT JOIN variacoes ON produtos.id = variacoes.id_produtos
WHERE ( MATCH(produtos.titulo) AGAINST ('$termo' IN BOOLEAN MODE) OR
MATCH(produtos.descricao) AGAINST ('$termo' IN BOOLEAN MODE) OR
MATCH(variacoes.legenda) AGAINST ('$termo' IN BOOLEAN MODE) )
QRY;

		if($secao){
			switch ($secao) {
				case 'boutique':
					$query .= " AND secoes.slug = 'boutique' ORDER BY categorias.titulo";
					break;
				case 'alimentacao':
					$query .= " AND secoes.slug = 'alimentacao' ORDER BY categorias.titulo";
					break;
				case 'higiene_e_beleza':
					$query .= " AND secoes.slug = 'higiene_e_beleza' ORDER BY categorias.titulo";
					break;
				case 'farmacia':
					$query .= " AND secoes.slug = 'farmacia' ORDER BY categorias.titulo";
					break;
			}
		}

		$exec = $this->db->query($query)->result();

		foreach ($exec as $key => $value) {
			$qry_categoria = $this->db->get_where('rel_categorias_produtos', array('id_produtos' => $value->id))->result();
			$qry_titulo = $this->db->get_where('categorias', array('id' => $qry_categoria[0]->id_categorias))->result();
			$value->cat_titulo = $qry_titulo[0]->titulo;
		}

		return $exec;

	}

	function pegarPorCategoria($campo, $identificador){
$query = <<<STR
SELECT DISTINCT produtos.* FROM produtos
LEFT JOIN rel_categorias_produtos ON produtos.id = rel_categorias_produtos.id_produtos
LEFT JOIN categorias ON rel_categorias_produtos.id_categorias = categorias.id
WHERE categorias.$campo = '$identificador'
STR;
		return $this->db->query($query)->result();
	}

	/* Função para Front-End */
	function pegarPorSecao($campo, $identificador, $limit = false){
$query = <<<STR
SELECT DISTINCT produtos.* FROM produtos
LEFT JOIN rel_categorias_produtos ON produtos.id = rel_categorias_produtos.id_produtos
LEFT JOIN categorias ON rel_categorias_produtos.id_categorias = categorias.id
LEFT JOIN secoes ON categorias.id_secoes = secoes.id
WHERE secoes.$campo = '$identificador' AND
produtos.publicar = 1
STR;
		
		if($limit !== false)
			$query .= " LIMIT 0, $limit";

		$retorno = $this->db->query($query)->result();
		foreach ($retorno as $key => $value) {
			$qry_img = $this->db->order_by('ordem', 'asc')->get_where('variacoes', array('id_produtos' => $value->id))->result();
			$value->imagem = (isset($qry_img[0])) ? $qry_img[0]->imagem : FALSE;
			$value->link = $this->gerarLink($value->id);
		}

		return $retorno;
	}

	function pegarRelacionados($filtro_secao, $filtro_categoria, $filtro_animais, $id){
		$query = <<<QRY
SELECT DISTINCT produtos.* FROM produtos
LEFT JOIN rel_categorias_produtos ON produtos.id = rel_categorias_produtos.id_produtos
LEFT JOIN categorias ON rel_categorias_produtos.id_categorias = categorias.id
LEFT JOIN secoes ON categorias.id_secoes = secoes.id
LEFT JOIN rel_animais_produtos ON rel_animais_produtos.id_produtos = produtos.id
LEFT JOIN animais ON rel_animais_produtos.id_animais = animais.id
QRY;
	
		$and = FALSE;

		if ($filtro_secao != 'todos') {
			$query .= " WHERE secoes.slug = '$filtro_secao'";
			$and = TRUE;
		}

		if($filtro_categoria != 'todos'){
			if($and){
				$query .= " AND categorias.slug = '$filtro_categoria'";
			}else{
				$query .= " WHERE categorias.slug = '$filtro_categoria'";
				$and = TRUE;		
			}
		}

		if($filtro_animais != 'todos'){
			if($and){
				$query .= " AND animais.slug = '$filtro_animais'";
			}else{
				$query .= " WHERE animais.slug = '$filtro_animais'";
			}
		}

		$query .= " AND produtos.id != $id LIMIT 0, 8";
		

		return $this->db->query($query)->result();
	}

	function pegarComFiltros($filtro_secao, $filtro_categoria, $filtro_animais, $num_resultados = FALSE, $per_page = 0, $pagina = 0){
		$query = <<<QRY
SELECT DISTINCT produtos.* FROM produtos
LEFT JOIN rel_categorias_produtos ON produtos.id = rel_categorias_produtos.id_produtos
LEFT JOIN categorias ON rel_categorias_produtos.id_categorias = categorias.id
LEFT JOIN secoes ON categorias.id_secoes = secoes.id
LEFT JOIN rel_animais_produtos ON rel_animais_produtos.id_produtos = produtos.id
LEFT JOIN animais ON rel_animais_produtos.id_animais = animais.id
QRY;
	
		$and = FALSE;

		if ($filtro_secao != 'todos') {
			$query .= " WHERE secoes.slug = '$filtro_secao'";
			$and = TRUE;
		}

		if($filtro_categoria != 'todos'){
			if($and){
				$query .= " AND categorias.slug = '$filtro_categoria'";
			}else{
				$query .= " WHERE categorias.slug = '$filtro_categoria'";
				$and = TRUE;		
			}
		}

		if($filtro_animais != 'todos'){
			if($and){
				$query .= " AND animais.slug = '$filtro_animais'";
			}else{
				$query .= " WHERE animais.slug = '$filtro_animais'";
			}
		}
		
		$query .= " ORDER BY produtos.titulo ASC";

		if($per_page)
			$query .= " LIMIT $pagina, $per_page";

		return ($num_resultados) ? $this->db->query($query)->num_rows() : $this->db->query($query)->result();
	}

	function inserir(){
		
		$slug = url_title($this->input->post('titulo'), '_', TRUE);
		$teste_slug = $slug;

		$inicio = 1;
		while($this->db->get_where('produtos', array('slug' => $teste_slug))->num_rows() != 0){
			$teste_slug = $slug.'_'.$inicio;
			$inicio++;
		}
		$this->dados_tratados['slug'] = $teste_slug;

		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}

		$this->db->set('data_cadastro', Date('Y-m-d H:i:s'));

		$insert = $this->db->insert($this->tabela);
		
		$id = $this->db->insert_id();

		$categorias = $this->input->post('categorias');
		if($categorias):
			foreach ($categorias as $key => $value) {
				$this->db->set('id_produtos', $id)->set('id_categorias', $value)->insert('rel_categorias_produtos');
			}
		endif;

		$animais = $this->input->post('animais');
		if($animais):
			foreach ($animais as $key => $value) {
				$this->db->set('id_produtos', $id)->set('id_animais', $value)->insert('rel_animais_produtos');
			}
		endif;

		$imagem = $this->sobeImagem();
		if($imagem){
			$this->db->set('imagem', $imagem)
					 ->set('legenda', $this->input->post('legenda'))
					 ->set('id_produtos', $id)
					 ->insert('variacoes');
		}

		return $insert;
	}

	function alterar($id){

		$slug = url_title($this->input->post('titulo'), '_', TRUE);
		$teste_slug = $slug;

		$inicio = 1;
		while($this->db->get_where('produtos', array('slug' => $teste_slug))->num_rows() != 0){
			$teste_slug = $slug.'_'.$inicio;
			$inicio++;
		}
		$this->dados_tratados['slug'] = $teste_slug;
		
		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}
			$update = $this->db->where('id', $id)->update($this->tabela);
		}

		$this->db->where('id_produtos', $id)->delete('rel_categorias_produtos');

		$categorias = $this->input->post('categorias');
		if($categorias):
			foreach ($categorias as $key => $value) {
				$this->db->set('id_produtos', $id)->set('id_categorias', $value)->insert('rel_categorias_produtos');
			}
		endif;

		$this->db->where('id_produtos', $id)->delete('rel_animais_produtos');

		$animais = $this->input->post('animais');
		if($animais):
			foreach ($animais as $key => $value) {
				$this->db->set('id_produtos', $id)->set('id_animais', $value)->insert('rel_animais_produtos');
			}
		endif;

		return $update;		
	}

	function excluir($id){
		if($this->pegarPorId($id) !== FALSE){
			$this->db->where('id_produtos', $id)->delete('rel_categorias_produtos');
			$this->db->where('id_produtos', $id)->delete('rel_animais_produtos');
			$this->db->where('id_produtos', $id)->delete('variacoes');
			return $this->db->where('id', $id)->delete($this->tabela);
		}
	}

	function pegarCategorias($id = false){
		if($id){
			$qry = $this->db->get_where('categorias', array('id' => $id))->result();
			return (isset($qry[0])) ? $qry[0] : FALSE;
		}else{
			$query = $this->db->order_by('id_secoes', 'asc')->order_by('ordem', 'asc')->get('categorias')->result();
			foreach ($query as $key => $value) {
				$qry_secao = $this->db->get_where('secoes', array('id' => $value->id_secoes))->result();
				$value->nome_secao = $qry_secao[0]->titulo;
			}
			return $query;
		}
	}

	function pegarCategoriasPorSecao($id = false){
		if($id)
			return $this->db->get_where('categorias', array('id_secoes' => $id))->result();			
		else
			return $this->db->order_by('ordem', 'asc')->get('categorias')->result();
	}	

	function inserir_categoria(){

		$slug = url_title($this->input->post('titulo'), '_', TRUE);
		$teste_slug = $slug;

		$inicio = 1;
		while($this->db->get_where('categorias', array('slug' => $teste_slug))->num_rows() != 0){
			$teste_slug = $slug.'_'.$inicio;
			$inicio++;
		}
		$slug = $teste_slug;

		return $this->db->set('titulo', $this->input->post('titulo'))
						->set('slug', $slug)
						->set('id_secoes', $this->input->post('id_secoes'))
						->insert("categorias");
	}

	function alterar_categoria($id){

		$slug = url_title($this->input->post('titulo'), '_', TRUE);
		$teste_slug = $slug;

		$inicio = 1;
		while($this->db->get_where('categorias', array('slug' => $teste_slug))->num_rows() != 0){
			$teste_slug = $slug.'_'.$inicio;
			$inicio++;
		}
		$slug = $teste_slug;

		return $this->db->set('titulo', $this->input->post('titulo'))
						->set('slug', $slug)
						->set('id_secoes', $this->input->post('id_secoes'))
						->where('id', $id)
						->update("categorias");
	}

	function excluir_categoria($id){
		if($this->pegarCategorias($id) !== FALSE){
			$this->db->where('id_categorias', $id)->delete('rel_categorias_produtos');
			return $this->db->where('id', $id)->delete("categorias");
		}
	}

	function categoriasDoProduto($id_produtos){
		$retorno = array();
		$qry = $this->db->get_where('rel_categorias_produtos', array('id_produtos' => $id_produtos))->result();
		foreach ($qry as $key => $value) {
			array_push($retorno, $value->id_categorias);
		}
		return $retorno;
	}

	function animaisDoProduto($id_produtos, $detalhes = false){
		$retorno = array();
		$qry = $this->db->get_where('rel_animais_produtos', array('id_produtos' => $id_produtos))->result();
		foreach ($qry as $key => $value) {
			array_push($retorno, $value->id_animais);
		}

		if($detalhes){
			$retorno_detalhado = array();
			foreach ($retorno as $value) {
				$arr = array(
					'id' => $value,
					'detalhe' => $this->pegarAnimais($value)
				);
				array_push($retorno_detalhado, $arr);
			}
			return $retorno_detalhado;
		}else{
			return $retorno;	
		}
	}

	function geraBreadcrumb($secao_slug, $categoria_slug, $animais_slug){
		$retorno = "&raquo; exibindo artigos de ";

		if($categoria_slug == 'todos'){
			$link = "<a href='produtos/index/$secao_slug/todos' title='Todas as Categorias'>todas as categorias</a>";
		}else{
			$query = $this->db->get_where('categorias', array('slug' => $categoria_slug))->result();
			$link = "<a href='produtos/index/$secao_slug/".$query[0]->slug."' title='".$query[0]->titulo."'>".$query[0]->titulo."</a>";
		}

		$retorno .= $link." para ";

		if($animais_slug == 'todos'){
			$link = "<a href='produtos/index/$secao_slug/".$categoria_slug."/todos/' title='Todos os Animais'>todos os animais</a>";
		}else{
			$query = $this->db->get_where('animais', array('slug' => $animais_slug))->result();
			$link = "<a href='produtos/index/$secao_slug/$categoria_slug/".$query[0]->slug."' title='".$query[0]->titulo."'>".$query[0]->titulo."</a>";
		}		
		$retorno .= $link;

		return $retorno;
	}

	function gerarLink($id_produtos, $categoria_slug = 'todos', $animais_slug = 'todos', $modal = FALSE){

		if($categoria_slug == 'todos'){			
			$qry_categorias = $this->db->get_where('rel_categorias_produtos', array('id_produtos' => $id_produtos))->result();

			if(!isset($qry_categorias[0]))
				redirect('home');

			$detalhe_categoria = $this->db->get_where('categorias', array('id' => $qry_categorias[0]->id_categorias))->result();

			$usar = $detalhe_categoria[0]->slug;
		}else{
			$usar = $categoria_slug;
		}
		
$query = <<<QRY
SELECT produtos.slug as 'produtos_slug', secoes.slug as 'secao_slug', categorias.slug as 'categoria_slug' FROM produtos
LEFT JOIN rel_categorias_produtos ON produtos.id = rel_categorias_produtos.id_produtos
LEFT JOIN categorias ON rel_categorias_produtos.id_categorias = categorias.id
LEFT JOIN secoes ON categorias.id_secoes = secoes.id
WHERE produtos.id = $id_produtos
QRY;
		$exec = $this->db->query($query)->result();

		$secao_slug     = $exec[0]->secao_slug;
		$categoria_slug = $usar;
		$animais_slug   = $animais_slug;
		$produto_slug   = $exec[0]->produtos_slug;

		return ($modal) ? "produtos/modal/$secao_slug/$categoria_slug/$animais_slug/$produto_slug" : "produtos/detalhes/$secao_slug/$categoria_slug/$animais_slug/$produto_slug";
	}

	function pegarVariacoes($id_produtos){
		return $this->db->order_by('ordem', 'asc')->get_where('variacoes', array('id_produtos' => $id_produtos))->result();
	}

	function pegaTitulo($tabela, $slug, $marcar = true){
		if($slug != 'todos'){
			$qry = $this->db->get_where($tabela, array('slug' => $slug))->result();
			return ($marcar) ? "<span class='laranja'>".ucfirst(mb_strtolower($qry[0]->titulo)).'</span>' : $qry[0]->titulo;
		}else{
			return ($marcar) ?  "<span class='laranja'>Todas as Seções</span>" : "Todos";
		}
	}

	function pegarSecoes($id = false){
		if($id){
			$qry = $this->db->get_where('secoes', array('id' => $id))->result();
			return (isset($qry[0])) ? $qry[0] : FALSE;
		}else
			return $this->db->order_by('id', 'asc')->get('secoes')->result();
	}

	function pegarAnimais($id = false){
		if($id){
			$qry = $this->db->get_where('animais', array('id' => $id))->result();
			return (isset($qry[0])) ? $qry[0] : FALSE;
		}else
			return $this->db->order_by('id', 'asc')->get('animais')->result();		
	}

	function imagens($id_parent, $id_imagem = FALSE){
		if(!$id_imagem){
			return $this->db->order_by('ordem', 'asc')->get_where($this->tabela_imagens, array('id_produtos' => $id_parent))->result();
		}else{
			$query = $this->db->order_by('ordem', 'asc')->get_where($this->tabela_imagens, array('id' => $id_imagem))->result();
			if(isset($query[0]))
				return $query[0];
			else
				return FALSE;
		}
	}

	function inserirImagem(){
		$imagem = $this->sobeImagem();
		if($imagem !== FALSE){
			$this->db->set('imagem', $imagem)
					 ->set('legenda', $this->input->post('legenda'));

			return $this->db->set('id_produtos', $this->input->post('id_parent'))->insert($this->tabela_imagens);
		}else
			return false;
	}

	function editarImagem($id_imagem){
		$imagem = $this->sobeImagem();
		
		if($imagem !== FALSE)
			$this->db->set('imagem', $imagem);
		
		$this->db->set('legenda', $this->input->post('legenda'));
		
		return  $this->db->where('id', $id_imagem)->update($this->tabela_imagens);
	}

	function excluirImagem($id_imagem){
		$imagem = $this->db->get_where($this->tabela_imagens, array('id' => $id_imagem))->result();
		@unlink('../'.$this->imagemOriginal['dir'].$imagem[0]->imagem);
		@unlink('../'.$this->imagemThumb['dir'].$imagem[0]->imagem);
		return $this->db->delete($this->tabela_imagens, array('id' => $id_imagem));
	}

	function sobeImagem(){
		$this->load->library('upload');

		$original = array(
			'dir' => '_imgs/produtos/',
			'campo' => 'userfile'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
		        
	        	$this->image_moo
	                ->load($original['dir'].$filename)
	                ->set_background_colour('#fff')
	                ->resize(440, 440, TRUE)
	                ->save($original['dir'].$filename, TRUE)
	                ->resize(110,110, TRUE)
	                ->save($original['dir'].'thumbs/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}

	function idSecao($slug){
		$query = $this->db->get_where('secoes', array('slug' => $slug))->result();
		return $query[0]->id;
	}

}