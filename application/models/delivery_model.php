<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Delivery_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'delivery';

		$this->dados = array('frase_abertura', 'politica');
		$this->dados_tratados = array();
	}

	function pegarBairros(){
		return $this->db->order_by('titulo', 'asc')->get('delivery_bairros')->result();
	}
}