<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slides_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'slides';

		$this->dados = array('imagem', 'texto', 'destino');
		$this->dados_tratados = array(
			'imagem' => $this->sobeImagem()
		);
	}

	function pegarTodos(){
		return $this->db->order_by('ordem', 'asc')->get($this->tabela)->result();
	}

	function sobeImagem(){
		$this->load->library('upload');

		$original = array(
			'campo' => 'userfile',
			'dir' => '_imgs/slides/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	                ->load($original['dir'].$filename)
	                ->resize_crop(430, 280)
	                ->save($original['dir'].$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	

}