<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Novidades_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'novidades';

		$this->dados = array('titulo', 'slug', 'data', 'olho', 'texto', 'imagem', 'cor');
		$this->dados_tratados = array(
			'data' => formataData($this->input->post('data'), 'br2mysql'),
			'imagem' => $this->sobeImagem()
		);
	}

	function pegarTodos(){
		return $this->db->order_by('data', 'desc')->get($this->tabela)->result();
	}

	function linkProximo($data_atual){
		$qry = $this->db->query("SELECT * FROM novidades WHERE data > '$data_atual' ORDER BY data, id DESC LIMIT 0, 1")->result();

		if(isset($qry[0]))
			return "<a href='novidades/detalhes/0/".$qry[0]->slug."' title='".$qry[0]->titulo."'>próxima notícia &raquo;</a>";
		else
			return FALSE;
	}

	function linkAnterior($data_atual){
		$qry = $this->db->query("SELECT * FROM novidades WHERE data < '$data_atual' ORDER BY data DESC, id ASC LIMIT 0, 1")->result();

		if(isset($qry[0]))
			return "<a href='novidades/detalhes/0/".$qry[0]->slug."' title='".$qry[0]->titulo."'>&laquo; notícia anterior</a>";
		else
			return FALSE;
	}

	function inserir(){

		$slug = url_title($this->input->post('titulo'), '_', TRUE);
		$teste_slug = $slug;

		$inicio = 1;
		while($this->db->get_where($this->tabela, array('slug' => $teste_slug))->num_rows() != 0){
			$teste_slug = $slug.'_'.$inicio;
			$inicio++;
		}
		$this->dados_tratados['slug'] = $teste_slug;

		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}
		return $this->db->insert($this->tabela);
	}

	function alterar($id){

		$slug = url_title($this->input->post('titulo'), '_', TRUE);
		$teste_slug = $slug;

		$inicio = 1;
		while($this->db->get_where($this->tabela, array('slug' => $teste_slug))->num_rows() != 0){
			$teste_slug = $slug.'_'.$inicio;
			$inicio++;
		}
		$this->dados_tratados['slug'] = $teste_slug;		

		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}
			return $this->db->where('id', $id)->update($this->tabela);
		}
	}	

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/novidades/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	        		->load($original['dir'].$filename)
	                ->resize(300, 600)
	                ->save($original['dir'].$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	

}