<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'contato';

		$this->dados = array('telefone', 'endereco', 'google_maps');
		$this->dados_tratados = array(
			'google_maps' => limpaGMaps($this->input->post('google_maps'), FALSE)
		);
	}

}