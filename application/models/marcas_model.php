<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Marcas_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'marcas';

		$this->dados = array('titulo', 'tagline', 'categoria', 'imagem');
		$this->dados_tratados = array(
			'imagem' => $this->sobeImagem()
		);
	}

	function sobeImagem(){
		$this->load->library('upload');

		$original = array(
			'campo' => 'userfile',
			'dir' => '_imgs/marcas/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	        		->set_background_colour('#FFF')
	                ->load($original['dir'].$filename)
	                ->resize(150, 150, TRUE)
	                ->save($original['dir'].$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	

}