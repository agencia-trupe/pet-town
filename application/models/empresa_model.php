<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresa_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'empresa';

		$this->dados = array('titulo', 'texto', 'imagem1', 'imagem2', 'imagem3');
		$this->dados_tratados = array(
			'imagem1' => $this->sobeImagem('userfile1'),
			'imagem2' => $this->sobeImagem('userfile2'),
			'imagem3' => $this->sobeImagem('userfile3')
		);
	}


	function alterar($id){
		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}

			if($this->input->post('rmv1'))
				$this->db->set('imagem1', '');

			if($this->input->post('rmv2'))
				$this->db->set('imagem2', '');

			if($this->input->post('rmv3'))
				$this->db->set('imagem3', '');

			return $this->db->where('id', $id)->update($this->tabela);
		}
	}

	function sobeImagem($campo){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/empresa/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	        		->set_background_colour('#FFF')
	                ->load($original['dir'].$filename)
	                ->resize(1000,1000)
	                ->save($original['dir'].$filename, TRUE)
	                ->resize(250, 250, TRUE)
	                ->save($original['dir'].'thumbs/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	

}