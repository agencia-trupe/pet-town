<div class="centro <?=$filtro_secao?>">

	<div id="nav-animais">
		<div class="faixa">filtrar por:</div>
		<div class="links-container">
			<a href="produtos/index/<?=$filtro_secao?>/<?=$filtro_categoria?>/caes" title="Cães" class="link-caes <?if($filtro_animais=='caes')echo"ativo"?>">cães</a>
			<a href="produtos/index/<?=$filtro_secao?>/<?=$filtro_categoria?>/gatos" title="Gatos" class="link-gatos <?if($filtro_animais=='gatos')echo"ativo"?>">gatos</a>
			<a href="produtos/index/<?=$filtro_secao?>/<?=$filtro_categoria?>/aves" title="Aves" class="link-aves <?if($filtro_animais=='aves')echo"ativo"?>">aves</a>
			<a href="produtos/index/<?=$filtro_secao?>/<?=$filtro_categoria?>/peixes" title="Peixes" class="link-peixes <?if($filtro_animais=='peixes')echo"ativo"?>">peixes</a>
			<a href="produtos/index/<?=$filtro_secao?>/<?=$filtro_categoria?>/outros" title="Outros" class="link-outros <?if($filtro_animais=='outros')echo"ativo"?>">outros</a>
		</div>
	</div>

	<h1><?=$titulo_secao?> <a href="produtos/index/<?=$filtro_secao?>/<?=$filtro_categoria?>/todos" title="Ver Todos">&raquo; exibir todos &laquo;</a></h1>

	<div class="lista-categorias">
		<?php foreach ($categorias as $key => $value): ?>
			<a href="produtos/index/<?=$filtro_secao?>/<?=$value->slug?>/<?=$filtro_animais?>" class="<?=$filtro_secao?> <?if($value->slug==$filtro_categoria)echo" ativo"?>" title="<?=$value->titulo?>"><?=$value->titulo?></a>
		<?php endforeach ?>
	</div>

	<div id="detalhe-produto" class="<?=$filtro_secao?>">

		<div id="imagem-container">
			<?php if ($detalhe->imagem): ?>
				<div id="imagem-ampliada"><img src="_imgs/produtos/<?=$detalhe->imagem?>"></div>
				<div id="legenda-imagem"><?=$detalhe->legenda?></div>	
			<?php else: ?>
				<div id="imagem-ampliada"><img src="_imgs/layout/noimage.gif"></div>
				<div id="legenda-imagem"></div>	
			<?php endif ?>			
		</div>
		
		<div id="descricao">
			<h2><?=$detalhe->titulo?></h2>

			<h4>características:</h4>
			<?=$detalhe->descricao?>

			<div class="indicacoes-animais <?=$filtro_secao?>">
				indicado para:&nbsp;&nbsp;&nbsp;
				<?php foreach ($detalhe->animais as $key => $value): ?>
					<img src="_imgs/layout/<?=$value['detalhe']->icone?>">
				<?php endforeach ?>
			</div>
			
			
			HTML5:<br>
			<div class="fb-like" style="margin-bottom:20px" data-href="<?=current_url()?>" data-send="false" data-layout="button_count" data-width="500" data-show-faces="false" data-font="segoe ui"></div>
			
			<!--
			IFRAME:<br>
			<iframe class="fb-like" style="margin-bottom:20px" src="//www.facebook.com/plugins/like.php?href=<?=urlencode(current_url())?>&amp;send=false&amp;layout=button_count&amp;width=500&amp;show_faces=false&amp;font=segoe+ui&amp;colorscheme=light&amp;action=like&amp;height=21&amp;appId=419942241422773" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:500px; height:21px;" allowTransparency="true"></iframe>
			-->

			<?php if ($detalhe->imagens): ?>
				<h4>variações:</h4>
				<?php foreach ($detalhe->imagens as $key => $value): ?>
					<a class="variacoes" href="_imgs/produtos/<?=$value->imagem?>" title="<?=$value->legenda?>"><img src="_imgs/produtos/thumbs/<?=$value->imagem?>"></a>		
				<?php endforeach ?>
			<?php endif ?>
			
		</div>

	</div>

	<div class="lista-produtos det">

		<?php if ($registros): ?>
			
			<div id="breadcrumbs">
				<?=$breadcrumbs?>
			</div>
		
			<?php foreach ($registros as $key => $prod): ?>

				<?php if ($key < 8 && $prod->id != $detalhe->id): ?>
				
					<a href="<?=$prod->link?>" class="link" title="<?=$prod->titulo?>">
						<div class="secao <?=$filtro_secao?>"><?=$titulo_secao?></div>
						<?php if ($prod->imagem): ?>
							<img src="_imgs/produtos/thumbs/<?=$prod->imagem?>">
						<?php else: ?>
							<img src="_imgs/layout/noimage.gif">
						<?php endif ?>
						<div class="titulo"><?=$prod->titulo?></div>
						<div class="overlay <?=$filtro_secao?>"></div>
						<div class="overlay-texto <?=$filtro_secao?>">ver detalhes</div>
					</a>

				<?php endif ?>

			<?php endforeach ?>

		<?php endif ?>

	</div>

</div>