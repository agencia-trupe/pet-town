<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Pet Town</title>
  <meta name="description" content="">
  <meta name="keywords" content="" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2012 Trupe Design" />

  <meta name="viewport" content="width=980,initial-scale=1">

  <meta property="og:title" content="<?=$detalhe->titulo?>"/>
  <meta property="og:site_name" content="Pet Town"/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="<?=$detalhe->imagem_facebook?>"/>
  <meta property="og:url" content="<?=current_url()?>"/>
  <meta property="og:description" content="<?=strip_tags($detalhe->descricao)?>"/>
  <meta property="fb:admins" content="100002297057504" />
  <meta property="fb:app_id" content="419942241422773" />

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>
  
  <link href='http://fonts.googleapis.com/css?family=PT+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

  <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>

  <?CSS(array('reset', 'base', 'fontface/stylesheet', 'fancybox/fancybox', 'produtos'))?>   

  <?if(ENVIRONMENT == 'development'):?>
    
    <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.8.0.min'))?>
    
  <?else:?>

    <?JS(array('modernizr-2.0.6.min', $this->router->class, $load_js))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

  <?endif;?>

</head>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=419942241422773";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<body class="modal">

<div class="centro">

  	<h1 class="<?=$filtro_secao?>"><?=$titulo_secao?></h1> <h2 class="<?=$filtro_secao?>"><?=$titulo_categoria?></h2>

		<div id="detalhe-produto" class="<?=$filtro_secao?>">

			<div id="imagem-container">
				<?php if ($detalhe->imagem): ?>
          <div id="imagem-ampliada"><img src="_imgs/produtos/<?=$detalhe->imagem?>"></div>
          <div id="legenda-imagem"><?=$detalhe->legenda?></div>
        <?php else: ?>
          <div id="imagem-ampliada"><img src="_imgs/layout/noimage.gif"></div>
          <div id="legenda-imagem"></div> 
        <?php endif ?>
			</div>
			
			<div id="descricao">
				<h2><?=$detalhe->titulo?></h2>

				<h4>características:</h4>
				<?=$detalhe->descricao?>

				<div class="indicacoes-animais <?=$filtro_secao?>">
					indicado para:&nbsp;&nbsp;&nbsp;
					<?php foreach ($detalhe->animais as $key => $value): ?>
						<img src="_imgs/layout/<?=$value['detalhe']->icone?>">
					<?php endforeach ?>
				</div>

        <div class="fb-like" style="margin-bottom:20px" data-href="<?=current_url()?>" data-send="false" data-layout="button_count" data-width="300" data-show-faces="false" data-font="lucida grande"></div>

				<?php if ($detalhe->imagens): ?>
					<h4>variações:</h4>
					<?php foreach ($detalhe->imagens as $key => $value): ?>
						<a class="variacoes" href="_imgs/produtos/<?=$value->imagem?>" title="<?=$value->legenda?>"><img src="_imgs/produtos/thumbs/<?=$value->imagem?>"></a>		
					<?php endforeach ?>
				<?php endif ?>
				
			</div>

		</div>

</div>

  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>

  <?JS(array('cycle', 'dir.hover', 'jgestures.min', 'imagesloaded', 'isotope', 'fancybox','front'))?>
  
</body>
</html>