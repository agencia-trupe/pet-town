<div class="centro <?=$filtro_secao?>">

	<div id="nav-animais">
		<div class="faixa">filtrar por:</div>
		<div class="links-container">
			<a href="produtos/index/<?=$filtro_secao?>/<?=$filtro_categoria?>/caes" title="Cães" class="link-caes <?if($filtro_animais=='caes')echo"ativo"?>">cães</a>
			<a href="produtos/index/<?=$filtro_secao?>/<?=$filtro_categoria?>/gatos" title="Gatos" class="link-gatos <?if($filtro_animais=='gatos')echo"ativo"?>">gatos</a>
			<a href="produtos/index/<?=$filtro_secao?>/<?=$filtro_categoria?>/aves" title="Aves" class="link-aves <?if($filtro_animais=='aves')echo"ativo"?>">aves</a>
			<a href="produtos/index/<?=$filtro_secao?>/<?=$filtro_categoria?>/peixes" title="Peixes" class="link-peixes <?if($filtro_animais=='peixes')echo"ativo"?>">peixes</a>
			<a href="produtos/index/<?=$filtro_secao?>/<?=$filtro_categoria?>/outros" title="Outros" class="link-outros <?if($filtro_animais=='outros')echo"ativo"?>">outros</a>
		</div>
	</div>

	<h1><?=$titulo_secao?> <a href="produtos/index/<?=$filtro_secao?>/<?=$filtro_categoria?>/todos" title="Ver Todos">&raquo; exibir todos &laquo;</a></h1>

	<div class="lista-categorias">
		<?php foreach ($categorias as $key => $value): ?>
			<a href="produtos/index/<?=$filtro_secao?>/<?=$value->slug?>/<?=$filtro_animais?>" class="<?=$filtro_secao?> <?if($value->slug==$filtro_categoria)echo" ativo"?>" title="<?=$value->titulo?>"><?=$value->titulo?></a>
		<?php endforeach ?>
	</div>

	<div class="lista-produtos">

		<div id="breadcrumbs">
			<?=$breadcrumbs?>
		</div>

		<?php if ($registros): ?>

			<?php foreach ($registros as $key => $prod): ?>
				
				<a href="<?=$prod->link?>" class="link<?if(($key + 1)%5 == 0)echo " ultimo"?>" title="<?=$prod->titulo?>">
					<div class="secao <?=$filtro_secao?>"><?=$titulo_secao?></div>
					<?php if ($prod->imagem): ?>
						<img src="_imgs/produtos/thumbs/<?=$prod->imagem?>">
					<?php else: ?>
						<img src="_imgs/layout/noimage.gif">
					<?php endif ?>
					<div class="titulo"><?=$prod->titulo?></div>
					<div class="overlay <?=$filtro_secao?>"></div>
					<div class="overlay-texto <?=$filtro_secao?>">ver detalhes</div>
				</a>

			<?php endforeach ?>

			<?php if ($paginacao): ?>
				<div class="paginacao">
					<?=$paginacao?>
				</div>
			<?php endif ?>

		<?php else: ?>

			<h3 class="no-result">Nenhum Produto Encontrado!</h3>

		<?php endif ?>

	</div>

</div>