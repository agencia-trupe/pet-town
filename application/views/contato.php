<div class="centro">

	<h1>Contato</h1>

	<div class="container">

		<div class="coluna margem">

			<div class="telefone"><?=$contato[0]->telefone?></div>

			<div class="endereco"><?=nl2br($contato[0]->endereco)?></div>

			<?=viewGMaps($contato[0]->google_maps, 465, 465)?>

			<img class="streetviewimg" src="http://maps.googleapis.com/maps/api/streetview?size=465x180&location=Alameda+dos+Maracatins,+63&sensor=false">

		</div>

		<div class="coluna">

			<form method="post" action="contato/fale" id="form-contato" class="amarelo">

				<div class="fundo-colorido">

					<h2>Fale conosco</h2>

					<label>
						<input type="text" name="nome" placeholder="nome" id="contato-nome" value="<?=$this->session->flashdata('contato-nome')?>" required>
					</label>

					<label>
						<input type="email" name="email" placeholder="e-mail" id="contato-email" value="<?=$this->session->flashdata('contato-email')?>" required>
					</label>

					<label>
						<input type="text" name="telefone" placeholder="telefone" id="contato-telefone" value="<?=$this->session->flashdata('contato-telefone')?>">
					</label>

					<label>
						<textarea name="mensagem" placeholder="mensagem" id="contato-mensagem" required><?=$this->session->flashdata('contato-mensagem')?></textarea>
					</label>

				</div>

				<input type="submit" value="ENVIAR">

			</form>

			<form method="post" action="contato/trabalhe" id="form-trabalhe" class="verde" enctype="multipart/form-data">

				<div class="fundo-colorido">

					<div class="area-click">
						<h2>Trabalhe conosco</h2>
						<h3>Envie seu currículo!</h3>
						<div id="seta">&raquo;</div>
					</div>

					<div class="hid" id="hid-form">

						<label>
							<input type="text" name="nome" placeholder="nome" id="trabalhe-nome" required value="<?=$this->session->flashdata('trabalhe-nome')?>">
						</label>

						<label>
							<input type="email" name="email" placeholder="e-mail" id="trabalhe-email" required value="<?=$this->session->flashdata('trabalhe-email')?>">
						</label>

						<label>
							<input type="text" name="telefone" placeholder="telefone" id="trabalhe-telefone" value="<?=$this->session->flashdata('trabalhe-telefone')?>">
						</label>

						<label>
							<textarea name="mensagem" placeholder="mensagem ou observações [se houver]" id="trabalhe-mensagem"><?=$this->session->flashdata('trabalhe-mensagem')?></textarea>
						</label>

						<label id="file-input">
							<div id="fake-input">ANEXAR CURRÍCULO &raquo;</div>
							<input type="file" name="userfile" id="trabalhe-curriculo">
						</label>

					</div>

				</div>

				<input type="submit" class="hid" id="hid-submit" value="ENVIAR">

			</form>

		</div>

	</div>


</div>

<?php if ($this->session->flashdata('mensagem_erro')): ?>

	<script defer>

		$('document').ready( function(){

			<?php if ($this->session->flashdata('mensagem_erro')): ?>
				$('.main .centro .coluna .hid').removeClass('hid');
				$('.area-click').addClass('aberto');
			<?php endif ?>

			alerta($("#<?=$this->session->flashdata('campo')?>"), "<?=$this->session->flashdata('mensagem_erro')?>")

		});
	</script>
<?php endif ?>

<?php if ($this->session->flashdata('mensagem_sucesso_contato')): ?>
	<script defer>
		var alerta_sucesso = function(){
			var alerta = $("<div class='alerta-sucesso hid'><div>Sua mensagem foi enviada com sucesso.<br>Obrigado!</div></div>");
			$('body').append(alerta);
			alerta.removeClass('hid');
			setTimeout( function(){
				alerta.addClass('hid');
				setTimeout( function(){
					alerta.remove();
				}, 300);
			}, 5000);
		}	
		$('document').ready( function(){
			alerta_sucesso();
		});
	</script>
<?php endif ?>

<?php if ($this->session->flashdata('mensagem_sucesso_trabalhe')): ?>
	<script defer>
		var alerta_sucesso = function(){
			var alerta = $("<div class='alerta-sucesso hid'><div>Seu currículo foi enviado com sucesso.<br>Obrigado!</div></div>");
			$('body').append(alerta);
			alerta.removeClass('hid');
			setTimeout( function(){
				alerta.addClass('hid');
				setTimeout( function(){
					alerta.remove();
				}, 300);
			}, 5000);
		}	
		$('document').ready( function(){
			alerta_sucesso();
		});
	</script>	
<?php endif ?>