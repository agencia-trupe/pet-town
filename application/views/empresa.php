<div class="centro">

	<h1>Pet Town</h1>

	<div class="container">

		<div class="coluna esquerda">

			<div class="texto">

				<h2><?=$registro[0]->titulo?></h2>

				<?=$registro[0]->texto?>

			</div>

			<ul class="selos-animais">
				<li>
					<img src="_imgs/layout/menu-caes-branco.png" alt="cães">
					cães
				</li>
				<li>
					<img src="_imgs/layout/menu-gatos-branco.png" alt="gatos">
					gatos
				</li>
				<li>
					<img src="_imgs/layout/menu-aves-branco.png" alt="aves">
					aves
				</li>
				<li>
					<img src="_imgs/layout/menu-peixes-branco.png" alt="peixes">
					peixes
				</li>
				<li>
					<img src="_imgs/layout/menu-outros-branco.png" alt="outros">
					outros
				</li>
			</ul>

			<h3>Aqui tem tudo para todos os bichinhos de estimação!!!</h3>

		</div>

		<div class="coluna direita">

			<?php if ($registro[0]->imagem1): ?>
				<a rel="empresa" href="_imgs/empresa/<?=$registro[0]->imagem1?>" class="fancy"><img src="_imgs/empresa/thumbs/<?=$registro[0]->imagem1?>"></a>
			<?php endif ?>

			<?php if ($registro[0]->imagem2): ?>
				<a rel="empresa" href="_imgs/empresa/<?=$registro[0]->imagem2?>" class="fancy"><img src="_imgs/empresa/thumbs/<?=$registro[0]->imagem2?>"></a>
			<?php endif ?>

			<?php if ($registro[0]->imagem3): ?>
				<a rel="empresa" href="_imgs/empresa/<?=$registro[0]->imagem3?>" class="fancy"><img src="_imgs/empresa/thumbs/<?=$registro[0]->imagem3?>"></a>
			<?php endif ?>

		</div>

	</div>
</div>