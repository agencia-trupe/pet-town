<div class="centro">

	<h1>Marcas</h1>

	<h2>Algumas das nossas principais marcas!</h2>

</div>

<div class="showcase-marcas">

	<?php if ($marcas): ?>
		
		<?php foreach ($marcas as $key => $value): ?>
			
			<div class="container-marca">

				<div class="box">
					<div class="lado frente">
						<img src="_imgs/marcas/<?=$value->imagem?>">
					</div>
					<div class="lado tras">
						<div class="titulo"><?=$value->titulo?></div>
						<?php if ($value->tagline): ?>
							<div class="tagline"><?=$value->tagline?></div>	
						<?php endif ?>
						<?php if ($value->categoria): ?>
							<div class="categoria">&raquo; <?=$value->categoria?> &laquo;</div>	
						<?php endif ?>
					</div>
				</div>
				
			</div>

		<?php endforeach ?>

	<?php endif ?>

</div>