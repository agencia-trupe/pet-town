
  </div> <!-- fim da div main -->

  <footer>

    <div class="centro">

      <div class="coluna primeira">
        <div class="telefone">11 2628-0133</div>

        <div class="endereco">
          Alameda dos Maracatins, 63<br>
          Moema · São Paulo, SP<br>
          04089-010
        </div>
      </div>

      <div class="coluna segunda">
        <ul>
          <li>&raquo;&nbsp;<a href="home" title="Página Inicial">HOME</a></li>
          <li>&raquo;&nbsp;<a href="empresa" title="Sobre a Pet Town">PET TOWN</a></li>
          <li>&raquo;&nbsp;<a href="novidades" title="Novidades">NOVIDADES</a></li>
          <li>&raquo;&nbsp;<a href="galeria" title="Galeria de Imagens">GALERIA</a></li>
          <li>&raquo;&nbsp;<a href="delivery" title="Nosso Delivery">DELIVERY</a></li>
          <li>&raquo;&nbsp;<a href="marcas" title="Marcas que Trabalhamos">MARCAS</a></li>
          <li>&raquo;&nbsp;<a href="contato" title="Entre em Contato">CONTATO</a></li>
        </ul>
      </div>

      <div class="coluna terceira">
        <ul>
          <li>&raquo;&nbsp;<a href="produtos/index/boutique/todos" title="Boutique">BOUTIQUE</a></li>
          <li>&raquo;&nbsp;<a href="produtos/index/alimentacao/todos" title="Alimentação">ALIMENTAÇÃO</a></li>
          <li>&raquo;&nbsp;<a href="produtos/index/higiene_e_beleza/todos" title="Higiene &amp; Beleza">HIGIENE &amp; BELEZA</a></li>
          <li>&raquo;&nbsp;<a href="produtos/index/farmacia/todos" title="Farmácia">FARMÁCIA</a></li>
        </ul>
      </div>

      <div class="coluna quarta">
        <ul>
          <li>&raquo;&nbsp;<a href="produtos/index/todos/todos/caes" title="Cães">CÃES</a></li>
          <li>&raquo;&nbsp;<a href="produtos/index/todos/todos/gatos" title="Gatos">GATOS</a></li>
          <li>&raquo;&nbsp;<a href="produtos/index/todos/todos/aves" title="Aves">AVES</a></li>
          <li>&raquo;&nbsp;<a href="produtos/index/todos/todos/peixes" title="Peixes">PEIXES</a></li>
          <li>&raquo;&nbsp;<a href="produtos/index/todos/todos/outros" title="Outros">OUTROS</a></li>
        </ul>
      </div>

      <div class="coluna quinta">
        &copy; <?=Date('Y')?> PetTown<br>
        Todos os direitos reservados<br>
        <a href="http://www.trupe.net" target="_blank" title="Criação de sites e comunicação: Trupe Agência Criativa" id="link-trupe">Criação de sites e comunicação:<br><span>Trupe Agência Criativa</span><img src="_imgs/layout/trupe.png" alt="Trupe Agência Criativa"></a>
      </div>

    </div>
  
  </footer>
  
  
    <script>
      window._gaq = [['_setAccount','UA-39812285-1'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  
  <?JS(array('cycle', 'dir.hover', 'jgestures.min', 'imagesloaded', 'isotope', 'fancybox','front'))?>
  
</body>
</html>
