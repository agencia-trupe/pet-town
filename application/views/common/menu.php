<header class="header-<?=$this->router->class?>">

	<div class="centro">

		<a href="<?=base_url()?>" title="Página Inicial" id="link-home"><img src="_imgs/layout/marca.png" alt="<?=CLIENTE?>"></a>

		<div id="preload-imgs">
			<div class="img1"></div>
			<div class="img2"></div>
			<div class="img3"></div>
			<div class="img4"></div>
			<div class="img5"></div>
			<div class="img6"></div>
			<div class="img7"></div>
			<div class="img8"></div>
			<div class="img9"></div>
		</div>

		<nav>
			<ul>
				<li><a href="home" title="Página Inicial" id="mn-home" <?if($this->router->class=='home')echo" class='ativo'"?>>Home</a></li>
				<li><a href="empresa" title="Sobre a Pet Town" id="mn-empresa" <?if($this->router->class=='empresa')echo" class='ativo'"?>>Pet Town</a></li>
				<li><a href="novidades" title="Novidades" id="mn-novidades" <?if($this->router->class=='novidades')echo" class='ativo'"?>>Novidades</a></li>
				<li><a href="galeria" title="Galeria de Imagens" id="mn-galeria" <?if($this->router->class=='galeria')echo" class='ativo'"?>>Galeria</a></li>
				<li><a href="delivery" title="Nosso Delivery" id="mn-delivery" <?if($this->router->class=='delivery')echo" class='ativo'"?>>Delivery</a></li>
				<li><a href="marcas" title="Marcas que Trabalhamos" id="mn-marcas" <?if($this->router->class=='marcas')echo" class='ativo'"?>>Marcas</a></li>
				<li><a href="contato" title="Entre em Contato" id="mn-contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>Contato</a></li>
			</ul>
		</nav>

		<ul id="main-nav">

			<li class="abre-submenu boutique <?if(isset($filtro_secao) && $filtro_secao == 'boutique')echo" ativo"?>">
				boutique
				<?php if (sizeof($submenu_boutique) > 0): ?>
					<ul class="submenu hid">
						<?php foreach ($submenu_boutique as $key => $value): ?>
							<li <?if($key == 0)echo" class='primeiro'"?>><a href="produtos/index/boutique/<?=$value->slug?>" title="<?=$value->titulo?>"><?=$value->titulo?></a></li>	
						<?php endforeach ?>	
					</ul>
				<?php endif ?>
			</li>

			<li class="abre-submenu alimentacao <?if(isset($filtro_secao) && $filtro_secao == 'alimentacao')echo" ativo"?>">
				alimentação
				<?php if (sizeof($submenu_alimentacao) > 0): ?>
					<ul class="submenu hid">
						<?php foreach ($submenu_alimentacao as $key => $value): ?>
							<li <?if($key == 0)echo" class='primeiro'"?>><a href="produtos/index/alimentacao/<?=$value->slug?>" title="<?=$value->titulo?>"><?=$value->titulo?></a></li>
						<?php endforeach ?>
					</ul>
				<?php endif ?>
			</li>

			<li class="abre-submenu higiene <?if(isset($filtro_secao) && $filtro_secao == 'higiene_e_beleza')echo" ativo"?>">
				higiene & beleza
				<?php if (sizeof($submenu_higiene) > 0): ?>
					<ul class="submenu hid">
						<?php foreach ($submenu_higiene as $key => $value): ?>
							<li <?if($key == 0)echo" class='primeiro'"?>><a href="produtos/index/higiene_e_beleza/<?=$value->slug?>" title="<?=$value->titulo?>"><?=$value->titulo?></a></li>
						<?php endforeach ?>
					</ul>
				<?php endif ?>
			</li>

			<li class="ultimo abre-submenu farmacia <?if(isset($filtro_secao) && $filtro_secao == 'farmacia')echo" ativo"?>">
				farmácia
				<?php if (sizeof($submenu_farmacia) > 0): ?>
					<ul class="submenu hid">
						<?php foreach ($submenu_farmacia as $key => $value): ?>
							<li <?if($key == 0)echo" class='primeiro'"?>><a href="produtos/index/farmacia/<?=$value->slug?>" title="<?=$value->titulo?>"><?=$value->titulo?></a></li>
						<?php endforeach ?>
					</ul>
				<?php endif ?>
			</li>

		</ul>

		<div id="div-form-busca">
			<form method="post" action="busca" id="form-busca">
				<input type="text" name="termo" placeholder="buscar"><input type="image" src="_imgs/layout/lupa.png">
			</form>
		</div>

		<?php if ($this->router->class == 'home'): ?>
			
			<div id="slides">
				<div id="animate">

					<?php foreach ($slides as $key => $value): ?>

						<a href="<?=$value->destino?>" title="<?=$value->texto?>" class="slide <?if($key>0)echo" hid"?>">
							<img src="_imgs/slides/<?=$value->imagem?>">
							<div class="texto">
								<?=$value->texto?>
							</div>
						</a>

					<?php endforeach ?>

				</div>

				<div id="navegacao"></div>
			</div>

			<div id="chamada-home">
				<h1>Tudo para seu bichinho de<br> estimação você encontra aqui!!!</h1>				
			</div>

		<?php else: ?>
			
			<div id="frase-internas">
				Tudo para seu bichinho de<br>estimação você encontra aqui!!!
			</div>

		<?php endif ?>

	</div>
	
</header>

<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">