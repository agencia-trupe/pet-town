<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Pet Town</title>
  <meta name="description" content="Na Pet Town você encontra variedade, preços competitivos e atendimento exclusivo. Seu pet merece esse carinho!">
  <meta name="keywords" content="" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2012 Trupe Design" />

  <meta name="viewport" content="width=980,initial-scale=1">

  <meta property="og:title" content="<?=(isset($title) && $title) ? $title : "Pet Town Boutique"?>"/>
  <meta property="og:site_name" content="Pet Town"/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="<?=(isset($imagem) && $imagem) ? base_url('_imgs/produtos/'.$imagem) : base_url('_imgs/layout/img_facebook.png')?>"/>
  <meta property="og:url" content="<?=current_url()?>"/>
  <meta property="og:description" content="<?=(isset($descricao) && $descricao) ? strip_tags($descricao) : "Descrição Genérica"?>"/>
  <meta property="fb:admins" content="100002297057504" />

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>
  
  <link href='http://fonts.googleapis.com/css?family=PT+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

  <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>

  <?CSS(array('reset', 'base', 'fontface/stylesheet', 'fancybox/fancybox', $this->router->class, $load_css))?>   

  <?if(ENVIRONMENT == 'development'):?>
    
    <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.8.0.min', $this->router->class, $load_js))?>
    
  <?else:?>

    <?JS(array('modernizr-2.0.6.min', $this->router->class, $load_js))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

  <?endif;?>

</head>
<body>
  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=419942241422773";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

