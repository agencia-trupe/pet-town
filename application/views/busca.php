<div class="centro">

	<h1>Resultado da busca</h1>

	<h2>Exibindo resultados de : &laquo; <?=$termo?> &raquo;</h2>

	<?php if ($resultados_boutique): ?>

		<h3 class="boutique">Boutique</h3>

		<?php foreach ($resultados_boutique as $key => $prod): ?>
			
			<a href="<?=$prod->link?>" class="link<?if(($key + 1)%5 == 0)echo " ultimo"?>" title="<?=$prod->titulo?>">
				<div class="secao <?=$prod->filtro_secao?>"><?=$prod->cat_titulo?></div>
				<?php if ($prod->imagem): ?>
					<img src="_imgs/produtos/thumbs/<?=$prod->imagem?>">
				<?php else: ?>
					<img src="_imgs/layout/noimage.gif">
				<?php endif ?>
				<div class="titulo"><?=$prod->titulo?></div>
				<div class="overlay <?=$prod->filtro_secao?>"></div>
				<div class="overlay-texto <?=$prod->filtro_secao?>">ver detalhes</div>
			</a>

		<?php endforeach ?>

	<?php endif ?>


	<?php if ($resultados_alimentacao): ?>

		<h3 class="alimentacao">Alimentação</h3>

		<?php foreach ($resultados_alimentacao as $key => $prod): ?>
			
			<a href="<?=$prod->link?>" class="link<?if(($key + 1)%5 == 0)echo " ultimo"?>" title="<?=$prod->titulo?>">
				<div class="secao <?=$prod->filtro_secao?>"><?=$prod->cat_titulo?></div>
				<?php if ($prod->imagem): ?>
					<img src="_imgs/produtos/thumbs/<?=$prod->imagem?>">
				<?php else: ?>
					<img src="_imgs/layout/noimage.gif">
				<?php endif ?>
				<div class="titulo"><?=$prod->titulo?></div>
				<div class="overlay <?=$prod->filtro_secao?>"></div>
				<div class="overlay-texto <?=$prod->filtro_secao?>">ver detalhes</div>
			</a>

		<?php endforeach ?>		

	<?php endif ?>


	<?php if ($resultados_higiene): ?>

		<h3 class="higiene">Higiene e Beleza</h3>

		<?php foreach ($resultados_higiene as $key => $prod): ?>
			
			<a href="<?=$prod->link?>" class="link<?if(($key + 1)%5 == 0)echo " ultimo"?>" title="<?=$prod->titulo?>">
				<div class="secao <?=$prod->filtro_secao?>"><?=$prod->cat_titulo?></div>
				<?php if ($prod->imagem): ?>
					<img src="_imgs/produtos/thumbs/<?=$prod->imagem?>">
				<?php else: ?>
					<img src="_imgs/layout/noimage.gif">
				<?php endif ?>
				<div class="titulo"><?=$prod->titulo?></div>
				<div class="overlay <?=$prod->filtro_secao?>"></div>
				<div class="overlay-texto <?=$prod->filtro_secao?>">ver detalhes</div>
			</a>

		<?php endforeach ?>		

	<?php endif ?>


	<?php if ($resultados_farmacia): ?>

		<h3 class="farmacia">Farmácia</h3>

		<?php foreach ($resultados_farmacia as $key => $prod): ?>
			
			<a href="<?=$prod->link?>" class="link<?if(($key + 1)%5 == 0)echo " ultimo"?>" title="<?=$prod->titulo?>">
				<div class="secao <?=$prod->filtro_secao?>"><?=$prod->cat_titulo?></div>
				<?php if ($prod->imagem): ?>
					<img src="_imgs/produtos/thumbs/<?=$prod->imagem?>">
				<?php else: ?>
					<img src="_imgs/layout/noimage.gif">
				<?php endif ?>
				<div class="titulo"><?=$prod->titulo?></div>
				<div class="overlay <?=$prod->filtro_secao?>"></div>
				<div class="overlay-texto <?=$prod->filtro_secao?>">ver detalhes</div>
			</a>

		<?php endforeach ?>		

	<?php endif ?>

	<?php if (!$resultados_boutique && !$resultados_alimentacao && !$resultados_higiene && !$resultados_farmacia): ?>
		<h3 class="no-result">Nenhum Produto Encontrado!</h3>
	<?php endif ?>

</div>

<script defer>
$('document').ready( function(){

	$('.link').fancybox({
		'titleShow' : false,
		'width'     : 980,
		'height'	: 690,
		'padding'	: 0,
		'type'		: 'iframe',
		'overlayOpacity' : 0.8,
		'scrolling' : 'auto'
	});

});
</script>