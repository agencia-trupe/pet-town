<div class="centro">

	<h1>Novidades</h1>

	<div class="nov-topo <?=$registro->cor?>">

		<div class="data"><?=formataData($registro->data, 'mysql2br', TRUE)?></div>
		<h2><?=$registro->titulo?></h2>

	</div>

	<div class="detalhe">

		<?php if ($registro->imagem): ?>
			<img class="img-novidade" src="_imgs/novidades/<?=$registro->imagem?>">
		<?php endif ?>

		<div class="texto <?=$registro->cor?><?if($registro->imagem)echo" short"?>">
			<?=str_replace('../../../', '', $registro->texto)?>
		</div>

	</div>

	<div class="navegacao">

		<a href="novidades/index/<?=$voltar_pagina?>" class="link-voltar" title="voltar para o menu de notícias">&laquo; voltar para o menu de notícias</a>
		
		<div class="paginacao">
			<?php if ($anterior): ?>
				<?=$anterior?>
			<?php endif ?>
			<?php if ($proximo): ?>
				<?=$proximo?>
			<?php endif ?>
		</div>
	</div>

</div>