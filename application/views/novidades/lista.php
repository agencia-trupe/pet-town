<div class="centro">

	<h1>Novidades</h1>

	<?php if ($novidades): ?>

		<?php foreach ($novidades as $key => $value): ?>

			<div class="novidade">
			
				<?php if ($value->imagem): ?>
					<img src="_imgs/novidades/<?=$value->imagem?>">
				<?php endif ?>

				<a href="novidades/detalhes/<?=$pagina_atual?>/<?=$value->slug?>" title="<?=$value->titulo?>" class="<?=$value->cor?> <?if($value->imagem)echo" short"?>">
					<div class="data"><?=formataData($value->data, 'mysql2br', TRUE)?></div>
					<h2><?=$value->titulo?></h2>
					<div class="olho"><?=$value->olho?></div>
					<div class="ler-mais">ler mais &raquo;</div>
				</a>

			</div>
			
		<?php endforeach ?>

		<?php if ($paginacao): ?>
			<div id="paginacao">
				<?=$paginacao?>
			</div>
		<?php endif ?>
		
	<?php else: ?>

		<h3 class="no-result">Nenhuma novidade cadastrada!</h3>
		
	<?php endif ?>


</div>