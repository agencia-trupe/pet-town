<div class="centro">

	<h1>Delivery</h1>

	<div class="container">

		<div class="coluna esquerda">

			<div class="texto">
				<h2><?=$registro[0]->frase_abertura?></h2>
			</div>

			<div class="col">

				<div class="coluna-texto margem vermelho">
					<h4>Formas de pagamento:</h4>
					<ul>
						<li>Dinheiro</li>
						<li>Cartão de crédito e débito</li>
					</ul>
				</div>

				<div class="coluna-texto margem">
					<h4>Política de entrega:</h4>
					<?=$registro[0]->politica?>
				</div>

			</div>

			<div class="coluna-texto">
				<h4>Bairros atendidos:</h4>
				<?php if ($bairros): ?>
					<ul>
						<?php foreach ($bairros as $key => $value): ?>
							<li><?=$value->titulo?></li>
						<?php endforeach ?>
					</ul>
				<?php endif ?>
			</div>

			<h3>Variedade, preços competitivos e atendimento exclusivo você encontra aqui!</h3>

		</div>

		<div class="coluna direita">

			<div class="caixa escura">
				11 2628-0133
				<p>
					Contando com a assessoria de nossos atendentes, esclareça suas dúvidas e faça seu pedido.
				</p>
			</div>

			<img src="_imgs/layout/img-delivery.jpg" id="img-delivery">

		</div>

	</div>
</div>