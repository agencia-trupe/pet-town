<?php foreach ($amostra_produtos as $lista): ?>
	<?php foreach ($lista as $key => $value): ?>
		
		<?
		switch($key){
			case 'boutique':
				$div = "<div class='circulo boutique'>Conheça<br>nossa linha<br>completa de<br>acessórios na<br>BOUTIQUE</div>";
				$titulo = "Boutique";
				break;
			case 'alimentacao':
				$div = "<div class='circulo alimentacao'>Pet saudável<br>exige boa<br>ALIMENTAÇÃO.<br>Confira nossas<br>comidinhas!</div>";
				$titulo = "Alimentação";
				break;
			case 'higiene':
				$div = "<div class='circulo higiene'>Cuidados<br>de HIGIENE &<br>BELEZA não<br>podem faltar!<br>Confira!</div>";
				$titulo = "Higiene & Beleza";
				break;
			case 'farmacia':
				$div = "<div class='circulo farmacia'>Nossa<br>FARMÁCIA é<br>super completa.<br>Todo cuidado<br>para seu pet!</div>";
				$titulo = "Farmácia";
				break;
		}
		?>

		<div class="linha-chamada">

			<ul>
				<li class="primeiro"><?=$div?></li>
				<?php foreach ($value as $prod): ?>
					<li>
						<a href="<?=$prod->link?>" title="<?=$prod->titulo?>">
							<div class="secao <?=$key?>"><?=$titulo?></div>
							<?php if ($prod->imagem): ?>
								<img src="_imgs/produtos/thumbs/<?=$prod->imagem?>">
							<?php else: ?>
								<img src="_imgs/layout/noimage.gif">
							<?php endif ?>
							<div class="titulo"><?=$prod->titulo?></div>
							<div class="overlay <?=$key?>"></div>
							<div class="overlay-texto <?=$key?>">ver detalhes</div>
						</a>
					</li>
				<?php endforeach ?>
			</ul>

		</div>

	<?php endforeach ?>
<?php endforeach ?>


<div id="faixa-home">

	<div class="bg-cinza">

		<div class="centro">

			<div class="coluna esquerda">
				<h2>MAIS FACILIDADE PARA NAVEGAR</h2>

				<p>
					Dentro de cada divisão você ainda  <br>
					pode selecionar qual é o seu bichinho <br>
					de estimação para facilitar a busca <br>
					de produtos!
				</p>

				<ul>
					<li><a href="produtos/index/todos/todos/caes" title="cães" class="link-caes">cães</a></li>
					<li><a href="produtos/index/todos/todos/gatos" title="gatos" class="link-gatos">gatos</a></li>
					<li><a href="produtos/index/todos/todos/aves" title="aves" class="link-aves">aves</a></li>
					<li><a href="produtos/index/todos/todos/peixes" title="peixes" class="link-peixes">peixes</a></li>
					<li><a href="produtos/index/todos/todos/outros" title="outros" class="ultimo link-outros">outros</a></li>
				</ul>
			</div>

			<div class="coluna direita">

				<h2><a href="galeria" title="Visite a Galeria de Imagens!">VISITE NOSSA GALERIA DE IMAGENS! <img src="_imgs/layout/seta-redonda.png"></a></h2>

				<h3>Você pode enviar suas fotos também!</h3>

				<?php if ($amostra_galeria): ?>
					<ul>
						<?php foreach ($amostra_galeria as $key => $value): ?>
							<li <?if($key==0)echo" class='primeiro'"?>><a href="galeria" title="Visite nossa galeria de imagens!"><img src="_imgs/galerias/thumbs/<?=$value->imagem?>"></a></li>
						<?php endforeach ?>	
					</ul>
				<?php endif ?>
				
			</div>

		</div>

	</div>

</div>