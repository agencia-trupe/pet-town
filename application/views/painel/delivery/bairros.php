<h1>Delivery > Bairros</h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Informações de Delivery</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/index_bairros')?>" class="add active">Bairros Atendidos</a>
</div>

<div class="submenu borda-cinza">

	<span class="nome-filtro">Adicionar Bairro : <img src="css/painel/arrow_down.png" class="mostra-filtros"></span><br>
	
	<div class="hid imgform">

		<br>

		<form method="post" action="painel/delivery/inserirBairro" enctype="multipart/form-data">

			<label>Nome do Bairro<br>
			<input type="text" name="titulo"></label>

			<input type="submit" value="ENVIAR">

		</form>

	</div>
</div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Nome</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<tbody>

			<? foreach ($registros as $key => $value): ?>

				<tr class="tr-row" id="row_<?=$value->id?>">
					<td><?=$value->titulo?></td>
					<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluirBairro/'.$value->id)?>">Excluir</a></td>
				</tr>
				
			<? endforeach; ?>

		</tbody>

	</table>

<?else:?>

	<h2>Nenhum Bairro Cadastrado</h2>

<?endif;?>