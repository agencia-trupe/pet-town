<h1>Delivery</h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Informações de Delivery</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/index_bairros')?>" class="add">Bairros Atendidos</a>
</div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Frase</th>
				<th>Política</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<tbody>

			<? foreach ($registros as $key => $value): ?>

				<tr class="tr-row" id="row_<?=$value->id?>">
					<td><?=$value->frase_abertura?></td>
					<td><?=word_limiter($value->politica, 10)?></td>
					<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
				</tr>
				
			<? endforeach; ?>

		</tbody>

	</table>

<?endif;?>