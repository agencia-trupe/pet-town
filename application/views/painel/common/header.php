<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>Painel de Administração - <?=CLIENTE?></title>
	<meta name="robots" content="noindex, nofollow" />
	<meta name="author" content="Trupe Design" />
	<meta name="copyright" content="2012 True Design" />	
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<base href="<?= base_url('painel') ?>">
	<script> var BASE = "<?= base_url('painel') ?>"</script>  

	<?CSS(array('painel/painel', 'jquery-theme-lightness/jquery-ui-1.8.20.custom'))?>  
	<?JS(array('modernizr-2.0.6.min', 'jquery-1.6.4.min', 'tinymce/tiny_mce', 'jquery-ui-1.8.12.custom.min',
				'jquery.ui.datepicker-pt-BR', 'jquery.mtz.monthpicker','placeholder'))?>

</head>
<body <?if(isset($mostrarerro) && $mostrarerro) echo "class='mostrarerro'"?> <?if(isset($mostrarsucesso) && $mostrarsucesso) echo "class='mostrarsucesso'"?>>