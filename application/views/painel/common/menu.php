
<!-- Shell -->
<div id="shell">
	
	<!-- Header -->
	<div id="header">
		<h1><a href="<?=base_url('painel/home')?>">Painel - <?=CLIENTE?></a></h1>
		<div class="right">
			<p>Bem vindo, <strong><?=$this->session->userdata('username')?></strong></p>
			<p class="small-nav"><a href="<?=base_url('painel/usuarios')?>" <?if($this->router->class=='usuarios')echo " class='active'"?>>Usuários</a> / <a href="<?=base_url('painel/home/logout')?>">Log Out</a></p>
		</div>
	</div>
	<!-- End Header -->
	
	<!-- Navigation -->
	<div id="navigation">
		<ul>
		    <li><a href="<?=base_url('painel/home')?>" <?if($this->router->class=='home')echo " class='active'"?>><span>Início</span></a></li>
		    <li><a href="<?=base_url('painel/slides')?>" <?if($this->router->class=='slides')echo " class='active'"?>><span>Slides da Home</span></a></li>
		    <li><a href="<?=base_url('painel/produtos')?>" <?if($this->router->class=='produtos')echo " class='active'"?>><span>Produtos</span></a></li>
		    <li><a href="<?=base_url('painel/empresa')?>" <?if($this->router->class=='empresa')echo " class='active'"?>><span>A Empresa</span></a></li>
		    <li><a href="<?=base_url('painel/novidades')?>" <?if($this->router->class=='novidades')echo " class='active'"?>><span>Novidades</span></a></li>
		    <li><a href="<?=base_url('painel/meupet')?>" <?if($this->router->class=='meupet')echo " class='active'"?>><span>Meu Pet</span></a></li>
		    <li><a href="<?=base_url('painel/delivery')?>" <?if($this->router->class=='delivery')echo " class='active'"?>><span>Delivery</span></a></li>
		    <li><a href="<?=base_url('painel/marcas')?>" <?if($this->router->class=='marcas')echo " class='active'"?>><span>Marcas</span></a></li>
		    <li><a href="<?=base_url('painel/contato')?>" <?if($this->router->class=='contato')echo " class='active'"?>><span>Contato</span></a></li>
		</ul>
	</div>
	<!-- End Navigation -->
	
	<!-- Content -->
	<div id="content">

	<?if(isset($mostrarerro_mensagem) && $mostrarerro_mensagem):?>
		<div class="mensagem" id="erro"><?=$mostrarerro_mensagem?></div>
	<?elseif(isset($mostrarsucesso_mensagem) && $mostrarsucesso_mensagem):?>
		<div class="mensagem" id="sucesso"><?=$mostrarsucesso_mensagem?></div>
	<?endif;?>