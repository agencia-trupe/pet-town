<h1>Contato</h1>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Telefone<br>
		<input type="text" name="telefone" value="<?=$registro->telefone?>"></label>

		<label>Endereço<br>
		<textarea name="endereco" style="width:450px; height:160px; resize:none;"><?=$registro->endereco?></textarea></label>

		<label>Link do Google Maps<br>
		<input type="text" name="google_maps" value="<?=htmlentities($registro->google_maps)?>"></label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?else:?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Telefone<br>
		<input type="text" name="telefone"></label>

		<label>Endereço<br>
		<textarea name="endereco" style="width:450px; height:160px; resize:none;"></textarea></label>

		<label>Link do Google Maps<br>
		<input type="text" name="google_maps"></label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>