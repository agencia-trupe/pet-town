<h1>Contato</h1>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Telefone</th>
				<th>Endereço</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<tbody>

			<? foreach ($registros as $key => $value): ?>

				<tr class="tr-row" id="row_<?=$value->id?>">
					<td><?=$value->telefone?></td>
					<td><?=nl2br($value->endereco)?></td>
					<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
				</tr>
				
			<? endforeach; ?>

		</tbody>

	</table>

<?endif;?>