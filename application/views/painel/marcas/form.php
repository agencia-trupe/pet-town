<h1>Marcas</h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar Marcas</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir Marca</a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título da Marca<br>
		<input type="text" name="titulo" value="<?=$registro->titulo?>"></label>

		<label>Tagline da Marca (se existir)<br>
		<input type="text" name="tagline" value="<?=$registro->tagline?>"></label>

		<label>Tipo de Produto<br>
		<input type="text" name="categoria" value="<?=$registro->categoria?>"></label>

		<label>Imagem
			<br>

			<?php if ($registro->imagem): ?>
				<img src="_imgs/marcas/<?=$registro->imagem?>">
				<br>
			<?php endif ?>
			
			<input type="file" name="userfile">
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?else:?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título da Marca<br>
		<input type="text" name="titulo"></label>

		<label>Tagline da Marca (se existir)<br>
		<input type="text" name="tagline"></label>

		<label>Tipo de Produto<br>
		<input type="text" name="categoria"></label>

		<label>Imagem<br>
		<input type="file" name="userfile"></label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>