<h1>Marcas</h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar Marcas</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Marca</a>
</div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Título</th>
				<th>Tagline</th>
				<th>Tipo</th>
				<th>Imagem</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<tbody>

			<? foreach ($registros as $key => $value): ?>

				<tr class="tr-row" id="row_<?=$value->id?>">
					<td><?=$value->titulo?></td>
					<td><?=$value->tagline?></td>
					<td><?=$value->categoria?></td>
					<td><img style="width:150px;" src="_imgs/marcas/<?=$value->imagem?>"></td>
					<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
					<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
				</tr>
				
			<? endforeach; ?>

		</tbody>

	</table>

<?else:?>

	<h2>Nenhuma Marca Cadastrada</h2>

<?endif;?>