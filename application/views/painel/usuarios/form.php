<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar <?=$titulo?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir <?=$unidade?></a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>">

		<div id="dialog"></div>
		
		<label>Usuário<br>
		<input type="text" name="username" required autofocus value="<?=$registro->username?>"></label>

		<label>E-mail<br>
		<input type="email" name="email" value="<?=$registro->email?>"></label>

		<label>Senha<br>
		<input type="password" name="password" id="senha"></label>

		<label>Confirmar Senha<br>
		<input type="password" id="conf-senha"></label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

	<script defer>
		$('document').ready( function(){
			$('form').submit( function(){
				if($('#senha').val() != ''){
					if($('#senha').val() != $('#conf-senha').val()){
						$('#dialog').html('As senhas informadas não conferem.')
									.dialog( { resizable :false, title: 'Atenção!' });
						$('#senha').focus();
						return false;
					}
				}
			});
		});
	</script>	
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>">

		<div id="dialog"></div>
		
		<label>Usuário<br>
		<input type="text" name="username" required autofocus></label>

		<label>E-mail<br>
		<input type="email" name="email"></label>

		<label>Senha<br>
		<input type="password" name="password" id="senha" required></label>

		<label>Confirmar Senha<br>
		<input type="password" id="conf-senha" required></label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
	<script defer>
		$('document').ready( function(){
			$('form').submit( function(){
				if($('#senha').val() == '' || $('#conf-senha').val() == ''){
					$('#dialog').html('Informe a senha e a confirmação de senha corretamente.')
								.dialog( { resizable :false, title: 'Atenção!' });
					$('#senha').focus();
					return false;
				}
				if($('#senha').val() != $('#conf-senha').val()){
					$('#dialog').html('As senhas informadas não conferem.')
								.dialog( { resizable :false, title: 'Atenção!' });
					$('#senha').focus();
					return false;
				}
			});
		});
	</script>

<?endif ?>