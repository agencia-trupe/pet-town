<div id="login-box">

	<h1>Painel Administrativo - <?=CLIENTE?></h1>

	<form method="post" id="login-form" action="<?=base_url('painel/home/login')?>">

		<div class="mensagem" id="erro">Erro ao logar. Verifique Usuário e Senha</div>

		<input type="text" name="usuario" required autocomplete="off" autofocus placeholder="Usuário" class="placeholder">

		<input type="password" name="senha" required autocomplete="off" placeholder="Senha" class="placeholder">

		<input type="submit" value="LOGIN">

	</form>
	
</div>