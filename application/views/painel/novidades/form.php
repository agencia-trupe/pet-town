<h1>Novidades</h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar Novidades</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Novidade</a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título<br>
		<input type="text" name="titulo" value="<?=$registro->titulo?>" required></label>

		<label>Data<br>
		<input type="text" id="datepicker" name="data" required value="<?=formataData($registro->data, 'mysql2br')?>"></label>

		<label>Olho<br>
		<textarea name="olho" class="pequeno basico"><?=$registro->olho?></textarea></label>

		<label>Texto<br>
		<textarea name="texto" class="grande comimagem"><?=$registro->texto?></textarea></label>

		<label>Imagem<br>
			<?php if ($registro->imagem): ?>
				<img src="_imgs/novidades/<?=$registro->imagem?>"><br>	
			<?php endif ?>
			
		<input type="file" name="userfile"></label>

		<label>Cor<br>
			<select name="cor">
				<option value="verde" <?if($registro->cor == 'verde')echo" selected"?>>Verde</option>
				<option value="azul" <?if($registro->cor == 'azul')echo" selected"?>>Azul</option>
				<option value="vermelho" <?if($registro->cor == 'vermelho')echo" selected"?>>Vermelho</option>
				<option value="amarelo" <?if($registro->cor == 'amarelo')echo" selected"?>>Amarelo</option>
			</select>
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?else:?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título<br>
		<input type="text" name="titulo" required></label>

		<label>Data<br>
		<input type="text" id="datepicker" required name="data"></label>

		<label>Olho<br>
		<textarea name="olho" class="pequeno basico"></textarea></label>

		<label>Texto<br>
		<textarea name="texto" class="grande comimagem"></textarea></label>

		<label>Imagem<br>
		<input type="file" name="userfile"></label>

		<label>Cor<br>
			<select name="cor">
				<option value="verde">Verde</option>
				<option value="azul">Azul</option>
				<option value="vermelho">Vermelho</option>
				<option value="amarelo">Amarelo</option>
			</select>
		</label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>