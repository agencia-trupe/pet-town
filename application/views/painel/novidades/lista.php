<h1>Novidades</h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar Novidades</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Novidade</a>
</div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Data</th>
				<th>Título</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<tbody>

			<? foreach ($registros as $key => $value): ?>

				<tr class="tr-row" id="row_<?=$value->id?>">
					<td><?=formataData($value->data, 'mysql2br')?></td>
					<td><?=$value->titulo?></td>
					<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
					<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
				</tr>
				
			<? endforeach; ?>

		</tbody>

	</table>

<?endif;?>
