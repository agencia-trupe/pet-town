<div class="centro">
	<h1>Bem Vindo(a),</h1>

	<p>Dia <?=date('d')?> de <?=mes(date('m'), true)?> de <?=date('Y')?>.</p>

	<p>Pelo painel administrativo da trupe você pode controlar as principais funções e conteúdo do site.</p>

	<p>Qualquer Dúvida entrar em contato: <a href="mailto:contato@trupe.net">contato@trupe.net</a></p>
</div>