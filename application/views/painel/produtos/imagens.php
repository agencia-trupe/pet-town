<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista <?if($this->router->method=='index')echo' active'?>">Listar Produtos</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add <?if($this->router->method=='form')echo' active'?>">Inserir Produto</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/categorias')?>" class="lista <?if($this->router->method=='categorias')echo' active'?>">Listar Categorias</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form_categorias')?>" class="add <?if($this->router->method=='form_categorias')echo' active'?>">Inserir Categoria</a>
</div>

<div class="submenu borda-cinza">

	<span class="nome-filtro">Enviar Imagem : <img src="css/painel/arrow_down.png" class="mostra-filtros"></span><br>

	
	<?php if ($registro): ?>
	
		<div class="imgform">

			<br>

			<form method="post" action="painel/produtos/editarImagem/<?=$registro->id?>" enctype="multipart/form-data">

				<label>Imagem<br>
					<img src="_imgs/produtos/<?=$registro->imagem?>" style="max-width:300px;"><br>
				<input type="file" name="userfile"></label>

				<label>Legenda<br>
				<input type="text" name="legenda" value="<?=$registro->legenda?>"></label>

				<input type="hidden" name="id_parent" value="<?=$parent->id?>">

				<input type="submit" value="ALTERAR"> <input style="margin-left:20px;" type="button" class="voltar" value="CANCELAR EDIÇÃO">

			</form>

		</div>

	<?php else: ?>

		<div class="hid imgform">

			<br>

			<form method="post" action="painel/produtos/inserirImagem" enctype="multipart/form-data">

				<label>Imagem<br>
				<input type="file" name="userfile"></label>

				<label>Legenda<br>
				<input type="text" name="legenda"></label>

				<input type="hidden" name="id_parent" value="<?=$parent->id?>">

				<input type="submit" value="ENVIAR">

			</form>

		</div>
		
	<?php endif ?>

</div>

<?if($imagens):?>

	<table>

		<thead>
			<tr>
				<th>Imagens</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

	</table>

	<ul class="resultados">

		<? foreach ($imagens as $key => $value): ?>

			<li class="tr-row" id="row_<?=$value->id?>">
				<img src="_imgs/produtos/thumbs/<?=$value->imagem?>" style="max-width:200px;">
				<div><?=$value->legenda?></div>
				<a class="move" href="#">Ordenar</a>
				<a class="edit" href="<?=base_url('painel/'.$this->router->class.'/imagens/'.$value->id_produtos.'/'.$value->id)?>">Editar</a>
				<a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluirImagem/'.$value->id.'/'.$value->id_produtos)?>">Excluir</a>
			</li>
				
		<? endforeach; ?>

	</ul>

<?else:?>

	<h2>Nenhuma Imagem Cadastrada</h2>

<?endif;?>

<input style="margin-left:20px;" type="button" class="voltar" value="VOLTAR">

<style type="text/css">
	.resultados .tr-row{
		text-align:center;
		display: inline-block;
		*display:inline;
		zoom:1;
		vertical-align:top;
		width:240px;
		margin:5px;
		border:1px #ccc solid;
		border-radius:5px;
	}
	.resultados .tr-row img{
		display:block;
		margin:3px auto;
	}
	ul.resultados li a{
		font-size:10px;
		padding:0 5px 0 30px;
		margin:2px;
	}
</style>

<script defer>

	$('document').ready( function(){

		$('.move').click( function(e){ e.preventDefault(); })

	    $(".resultados").sortable({
	        update : function () {
	            serial = [];
	            $('.resultados').children('.tr-row').each(function(idx, elm) {
	                serial.push(elm.id.split('_')[1])
	            });
	            $.post(BASE+'/ajax/gravaOrdem', { data : serial , tabela : 'variacoes'}, function(retorno){
	            	//console.log(retorno);
	            });
	        },
	        helper: function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			},
			handle : $('.move')
	    }).disableSelection();

	});
</script>