<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista <?if($this->router->method=='index')echo' active'?>">Listar Produtos</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add <?if($this->router->method=='form')echo' active'?>">Inserir Produto</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/categorias')?>" class="lista <?if($this->router->method=='categorias')echo' active'?>">Listar Categorias</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form_categorias')?>" class="add <?if($this->router->method=='form_categorias')echo' active'?>">Inserir Categoria</a>
</div>

<div class="submenu borda-cinza">
	<span class="nome-filtro">Filtrar por Seção : <img src="css/painel/arrow_down.png" class="mostra-filtros"></span><br>
	<div class="filtros hid">
		<a href="<?=base_url('painel/'.$this->router->class.'/index/todos')?>" class="lista <?if($filtro_secao=='todos')echo' active'?>">Todos</a>
		<?php foreach ($secoes as $key => $value): ?>
			<a href="<?=base_url('painel/'.$this->router->class.'/index/'.$value->slug)?>" class="lista <?if($filtro_secao==$value->slug)echo' active'?>"><?=$value->titulo?></a>	
		<?php endforeach ?>
	</div>
</div>

<div class="submenu borda-cinza">
	<span class="nome-filtro">Filtrar por Categoria : <img src="css/painel/arrow_down.png" class="mostra-filtros"></span><br>
	<div class="filtros hid">
		<a href="<?=base_url('painel/'.$this->router->class.'/index/'.$filtro_secao.'/todos')?>" class="lista <?if($filtro_categoria=='todos')echo' active'?>">Todos</a>	
		<?php foreach ($categorias as $key => $value): ?>
			<a href="<?=base_url('painel/'.$this->router->class.'/index/'.$filtro_secao.'/'.$value->slug)?>" class="lista <?if($filtro_categoria==$value->slug)echo' active'?>"><?=$value->titulo?></a>	
		<?php endforeach ?>
	</div>
</div>

<div class="submenu borda-cinza">
	<span class="nome-filtro">Filtrar por Animais : <img src="css/painel/arrow_down.png" class="mostra-filtros"></span><br>
	<div class="filtros hid">
		<a href="<?=base_url('painel/'.$this->router->class.'/index/'.$filtro_secao.'/'.$filtro_categoria.'/todos')?>" class="lista <?if($filtro_animais=='todos')echo' active'?>">Todos</a>	
		<?php foreach ($animais as $key => $value): ?>
			<a href="<?=base_url('painel/'.$this->router->class.'/index/'.$filtro_secao.'/'.$filtro_categoria.'/'.$value->slug)?>" class="lista <?if($filtro_animais==$value->slug)echo' active'?>"><?=$value->titulo?></a>	
		<?php endforeach ?>
	</div>
</div>

<div class="submenu busca">
	Buscar Produto : <form action="painel/produtos/busca" method="post"><input type="text" name="termo"><input type="submit" value="OK">
</div>


<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Publicado</th>
				<th>Título</th>
				<th>Descrição</th>
				<th class="option-cell">Imagem</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<tbody>

			<? foreach ($registros as $key => $value): ?>

				<tr class="tr-row" id="row_<?=$value->id?>">
					<td>
						<?php if ($value->publicar): ?>
							<img src="css/painel/accept.png" style="display:block; margin:0 auto;">
						<?php else: ?>
							<img src="css/painel/cross.png" style="display:block; margin:0 auto;">
						<?php endif ?>
					</td>
					<td><?=$value->titulo?></td>
					<td><?=word_limiter($value->descricao, 15)?></td>
					<td>
						<?php if ($value->imagens && isset($value->imagens[0])): ?>
							<img src="_imgs/produtos/thumbs/<?=$value->imagens[0]->imagem?>" style="width:100px;">
						<?php else: ?>
							<img src="_imgs/layout/noimage.gif" style="width:100px;">
						<?php endif ?>
					</td>
					<td><a class="imagens" href="<?=base_url('painel/'.$this->router->class.'/imagens/'.$value->id)?>">Imagens/Variações</a></td>
					<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
					<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
				</tr>
				
			<? endforeach; ?>

		</tbody>

	</table>

<?else:?>

	<h2>Nenhum Produto Cadastrado</h2>

<?endif;?>
