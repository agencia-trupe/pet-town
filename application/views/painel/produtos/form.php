<h1><?=$titulo?></h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista <?if($this->router->method=='index')echo' active'?>">Listar Produtos</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add <?if($this->router->method=='form')echo' active'?>">Inserir Produto</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/categorias')?>" class="lista <?if($this->router->method=='categorias')echo' active'?>">Listar Categorias</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form_categorias')?>" class="add <?if($this->router->method=='form_categorias')echo' active'?>">Inserir Categoria</a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título do Produto *<br>
		<input type="text" name="titulo" value="<?=$registro->titulo?>" required></label>

		<label>Descrição do Produto<br>
		<textarea name="descricao" class="pequeno basico"><?=$registro->descricao?></textarea></label>

		<div style="width:325px; display:inline-block; vertical-align:top;">
			<label style="margin-bottom:0;">Categorias *</label>
			<?php foreach ($categorias as $key => $value): ?>
				<label style="margin-bottom:0;"><input type="checkbox" class="chk_cat" name="categorias[]" <?if(in_array($value->id, $arr_categorias))echo" checked"?> value="<?=$value->id?>"><?=$value->nome_secao?> - <?=$value->titulo?></label>
			<?php endforeach ?>
		</div>
		
		<div style="width:325px; display:inline-block; vertical-align:top;">
			<label style="margin-bottom:0;">Animais *</label>
			<?php foreach ($animais as $key => $value): ?>
				<label style="margin-bottom:0;"><input type="checkbox" class="chk_ani" name="animais[]" <?if(in_array($value->id, $arr_animais))echo" checked"?> value="<?=$value->id?>"><?=$value->titulo?></label>
			<?php endforeach ?>
		</div>

		<div style="border-top:1px #CCC solid; border-bottom:1px #CCC solid; margin:20px 0;">
			<label style="margin:10px 0;"><input type="checkbox" name="publicar" value="1" <?if($registro->publicar)echo" checked"?>>Publicar Produto</label>
		</div>

		<br>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?else:?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título do Produto *<br>
		<input type="text" name="titulo" required></label>

		<label>Descrição do Produto<br>
		<textarea name="descricao" class="pequeno basico"></textarea></label>

		<label>Imagem Principal do Produto<br>
		<input type="file" name="userfile"></label>

		<label>Legenda da Imagem Principal do Produto<br>
		<input type="text" name="legenda"></label>

		<div style="width:325px; display:inline-block; vertical-align:top;">
			<label style="margin-bottom:0;">Categorias *</label>
			<?php foreach ($categorias as $key => $value): ?>
				<label style="margin-bottom:0;"><input type="checkbox" class="chk_cat" name="categorias[]" value="<?=$value->id?>"><?=$value->nome_secao?> - <?=$value->titulo?></label>
			<?php endforeach ?>
		</div>
		
		<div style="width:325px; display:inline-block; vertical-align:top;">
			<label style="margin-bottom:0;">Animais *</label>
			<?php foreach ($animais as $key => $value): ?>
				<label style="margin-bottom:0;"><input type="checkbox" class="chk_ani" name="animais[]" value="<?=$value->id?>"><?=$value->titulo?></label>
			<?php endforeach ?>
		</div>

		<div style="border-top:1px #CCC solid; border-bottom:1px #CCC solid; margin:20px 0;">
			<label style="margin:10px 0;"><input type="checkbox" name="publicar" value="1">Publicar Produto</label>
		</div>

		<br>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>

<script defer>
$('document').ready( function(){

	$('form').submit( function(){

		var categorias_marcadas = $('.chk_cat:checked').length;
		var animais_marcados = $('.chk_ani:checked').length;

		if(categorias_marcadas == 0){
			alert('Selecione ao menos uma categoria para o produto!');
			return false;
		}

		if(animais_marcados == 0){
			alert('Selecione ao menos um tipo de animal para o produto!');
			return false;
		}

	});

});
</script>