<h1>A Empresa</h1>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Texto</th>
				<th>Imagens</th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<tbody>

			<? foreach ($registros as $key => $value): ?>

				<tr class="tr-row" id="row_<?=$value->id?>">
					<td><?=word_limiter($value->texto, 10)?></td>
					<td>
						<?php if ($value->imagem1): ?>
							<img style="width:150px;" src="_imgs/empresa/thumbs/<?=$value->imagem1?>">	
						<?php endif ?>
						<?php if ($value->imagem2): ?>
							<img style="width:150px;" src="_imgs/empresa/thumbs/<?=$value->imagem2?>">
						<?php endif ?>
						<?php if ($value->imagem3): ?>
							<img style="width:150px;" src="_imgs/empresa/thumbs/<?=$value->imagem3?>">
						<?php endif ?>
					</td>
					<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
				</tr>
				
			<? endforeach; ?>

		</tbody>

	</table>

<?endif;?>