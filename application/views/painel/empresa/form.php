<h1>A Empresa</h1>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título<br>
		<input type="text" name="titulo" value="<?=$registro->titulo?>"></label>

		<label>Texto<br>
		<textarea name="texto" class="grande basico"><?=$registro->texto?></textarea></label>

		<label>Imagem 1
			<br>

			<?php if ($registro->imagem1): ?>
				<img src="_imgs/empresa/thumbs/<?=$registro->imagem1?>">
				<br>
				<input type="checkbox" name="rmv1" value="1">Remover Imagem
				<br>
			<?php endif ?>
			
			<input type="file" name="userfile1">
		</label>

		<label>Imagem 2
			<br>

			<?php if ($registro->imagem2): ?>
				<img src="_imgs/empresa/thumbs/<?=$registro->imagem2?>">
				<br>
				<input type="checkbox" name="rmv2" value="1">Remover Imagem
				<br>
			<?php endif ?>
			
			<input type="file" name="userfile2"></label>

		<label>Imagem 3
			<br>

			<?php if ($registro->imagem3): ?>
				<img src="_imgs/empresa/thumbs/<?=$registro->imagem3?>">
				<br>
				<input type="checkbox" name="rmv3" value="1">Remover Imagem
				<br>
			<?php endif ?>

			<input type="file" name="userfile3">
		</label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?else:?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título<br>
		<input type="text" name="titulo"></label>

		<label>Texto<br>
		<textarea name="texto" class="grande basico"></textarea></label>

		<label>Imagem 1<br>
		<input type="file" name="userfile1"></label>

		<label>Imagem 2<br>
		<input type="file" name="userfile2"></label>

		<label>Imagem 3<br>
		<input type="file" name="userfile3"></label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>

<?endif ?>