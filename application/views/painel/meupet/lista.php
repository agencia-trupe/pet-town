<h1>Meu Pet</h1>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Aprovado</th>
				<th>Imagem</th>
				<th>Nome</th>
				<th>Legenda</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<tbody>

			<? foreach ($registros as $key => $value): ?>

				<tr class="tr-row" id="row_<?=$value->id?>">
					<td>
						<?php
						if ($value->publicar) {
							echo "<img src='css/painel/accept.png'>";
						} else {
							echo "<img src='css/painel/cross.png'>";
						}
						?>
					</td>
					<td>
						<img src="_imgs/galerias/thumbs/<?=$value->imagem?>" style="width:125px;">
					</td>
					<td><?=$value->nome?></td>
					<td><?=$value->legenda?></td>
					<td><a class="move" href="#">Mover</a></td>
					<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Visualizar</a></td>
					<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
				</tr>
				
			<? endforeach; ?>

		</tbody>

	</table>

<?else:?>

	<h2>Nenhum Pet Cadastrado</h2>

<?endif;?>

<style type="text/css">
	table tbody tr{
		width:100%;
	}
</style>

<script defer>

	$('document').ready( function(){

		$('.move').click( function(e){ e.preventDefault(); })

	    $("table tbody").sortable({
	        update : function () {
	            serial = [];
	            $('table tbody').children('tr').each(function(idx, elm) {
	                serial.push(elm.id.split('_')[1])
	            });
	            $.post(BASE+'/ajax/gravaOrdem', { data : serial , tabela : 'meupet'}, function(retorno){
	            	//console.log(retorno);
	            });
	        },
	        helper: function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			},
			handle : $('.move')
	    }).disableSelection();

	});
</script>
