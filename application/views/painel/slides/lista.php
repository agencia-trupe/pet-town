<h1>Slides da Home</h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista active">Listar Slides</a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add">Inserir Slide</a>
</div>

<?if($registros):?>

	<table>

		<thead>
			<tr>
				<th>Texto</th>
				<th>Imagem</th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
				<th class="option-cell"></th>
			</tr>
		</thead>

		<? foreach ($registros as $key => $value): ?>

			<tr class="tr-row" id="row_<?=$value->id?>">
				<td><?=$value->texto?></td>
				<td><img src="_imgs/slides/<?=$value->imagem?>" style="width:120px;"></td>
				<td><a class="move" href="#">Reordenar</a></td>
				<td><a class="edit" href="<?=base_url('painel/'.$this->router->class.'/form/'.$value->id)?>">Editar</a></td>
				<td><a class="delete" href="<?=base_url('painel/'.$this->router->class.'/excluir/'.$value->id)?>">Excluir</a></td>
			</tr>
			
		<? endforeach; ?>

	</table>

<?else:?>

	<h2>Nenhum Slide Cadastrado</h2>

<?endif;?>

<style type="text/css">
	table tbody tr{
		width:100%;
	}
</style>

<script defer>

	$('document').ready( function(){

		$('.move').click( function(e){ e.preventDefault(); })

	    $("table tbody").sortable({
	        update : function () {
	            serial = [];
	            $('table tbody').children('tr').each(function(idx, elm) {
	                serial.push(elm.id.split('_')[1])
	            });
	            $.post(BASE+'/ajax/gravaOrdem', { data : serial , tabela : 'slides'}, function(retorno){
	            	//console.log(retorno);
	            });
	        },
	        helper: function(e, ui) {
				ui.children().each(function() {
					$(this).width($(this).width());
				});
				return ui;
			},
			handle : $('.move')
	    }).disableSelection();

	});
</script>