<h1><?echo ($registro) ? 'Alterar ' : 'Inserir '?>Slides da Home</h1>

<div class="submenu">
	<a href="<?=base_url('painel/'.$this->router->class.'/index')?>" class="lista">Listar <?=$titulo?></a>
	<a href="<?=base_url('painel/'.$this->router->class.'/form')?>" class="add active">Inserir <?=$unidade?></a>
</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Texto<br>
		<input type="text" name="texto" required value="<?=$registro->texto?>"></label>

		<label>Destino do Link<br>
		<input type="text" name="destino" required value="<?=$registro->destino?>"></label>

		<label>Imagem<br>
		<img src="_imgs/slides/<?=$registro->imagem?>">
		</br>
		<input type="file" name="userfile"></label>

		<input type="submit" value="ALTERAR"> <input type="button" class="voltar" value="VOLTAR">
	</form>
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Texto<br>
		<input type="text" name="texto" required autofocus></label>

		<label>Destino do Link<br>
		<input type="text" name="destino" required></label>

		<label>Imagem<br>
		<input type="file" name="userfile" required></label>

		<input type="submit" value="INSERIR"> <input type="button" class="voltar" value="VOLTAR">
	</form>


<?endif ?>