<div class="centro">
	<h1>Galeria</h1>
	<h2>Meu Pet! <span class="pequeno">Uma galeria de clientes felizes e lindos!</span></h2>
	<div class="botao-form">
		<a href="galeria/enviar" title="Envie sua foto!">Envie sua foto para o nosso mural clicando aqui!</a>
	</div>
</div>

<div class="wide">

	<?php foreach ($registros as $key => $value): ?>

		<div class="portrait">
			<div class="frente lado">
				<img src="_imgs/galerias/<?=$value->imagem?>">
				<div class="nome"><?=$value->nome?></div>
				<div class="legenda"><?=$value->legenda?></div>
			</div>
		</div>

	<?php endforeach ?>

</div>

<div id="loader" class="hid">
	<div class="windows8">
		<div class="wBall" id="wBall_1">
			<div class="wInnerBall">
			</div>
		</div>
		<div class="wBall" id="wBall_2">
			<div class="wInnerBall">
			</div>
		</div>
		<div class="wBall" id="wBall_3">
			<div class="wInnerBall">
			</div>
		</div>
		<div class="wBall" id="wBall_4">
			<div class="wInnerBall">
			</div>
		</div>
		<div class="wBall" id="wBall_5">
			<div class="wInnerBall">
			</div>
		</div>
	</div>			
</div>	

<?php if ($total_resultados > $porpagina): ?>
	<div id="pegar-mais-imagens">
		<input type="hidden" id="por_pagina" value="<?=$porpagina?>">
		<input type="hidden" id="total_resultados" value="<?=$total_resultados?>">
		<a href="#" data-ultimo="<?=$porpagina?>" title="Ver mais Fotos">ver mais &raquo;</a>
	</div>	
<?php endif ?>

<?php if ($this->session->flashdata('mensagem_sucesso')): ?>

	<script defer>
	var alerta_sucesso = function(){
		var alerta = $("<div class='alerta-sucesso hid'><div>Sua foto foi enviada com sucesso e será publicada após aprovação.<br>Obrigado!</div></div>");
		$('body').append(alerta);
		alerta.removeClass('hid');
		setTimeout( function(){
			alerta.addClass('hid');
			setTimeout( function(){
				alerta.remove();
			}, 300);
		}, 5000);
	}
	$('document').ready( function(){
		alerta_sucesso();
	});
	</script>

<?php endif ?>