<div class="centro">
	<h1>Galeria</h1>
	<h2>Meu Pet! <span class="pequeno">Uma galeria de clientes felizes e lindos!</span></h2>

	<form method="post" action="galeria/submeterAprovacao" id="pet_form" enctype="multipart/form-data">

		<div class="coluna usuario">
			<h3>Preencha aqui os seus dados: *</h3>

			<label>
				Seu nome<br>
				<input type="text" name="nome" id="input-nome" value="<?=$this->session->flashdata('flash_nome')?>">
			</label>

			<label>
				Seu e-mail<br>
				<input type="email" name="email" id="input-email" value="<?=$this->session->flashdata('flash_email')?>">
			</label>

			<label>
				Sua idade<br>
				<input type="text" name="idade" id="input-idade" maxlength="3" value="<?=$this->session->flashdata('flash_idade')?>">
			</label>

			<label class="obs">
				* Seus dados serão utilizados apenas para cadastro e não serão divulgados no site.
			</label>

		</div>

		<div class="coluna pet">
			<h3>Preencha aqui os dados do seu pet:</h3>

			<label>
				Nome do seu pet<br>
				<input type="text" name="pet_nome" id="input-pet_nome" value="<?=$this->session->flashdata('flash_nome_pet')?>">
			</label>

			<label>
				Frase de legenda da foto<br>
				<input type="text" name="pet_legenda" id="input-pet_legenda" maxlength="140" value="<?=$this->session->flashdata('flash_legenda')?>">
			</label>

			<label class="offset">
				<div class="layer" id="fakeinput">ENVIAR IMAGEM</div>
				<input type="file" name="pet_imagem" id="input-pet_imagem" >
			</label>

		</div>

		<input type="submit" value="finalizar cadastro">

	</form>
</div>

<?php if ($this->session->flashdata('mensagem_erro')): ?>
	<script defer>
	$('document').ready( function(){
		alerta($("#<?=$this->session->flashdata('campo')?>"), "<?=$this->session->flashdata('mensagem_erro')?>")
	});
	</script>
<?php endif ?>