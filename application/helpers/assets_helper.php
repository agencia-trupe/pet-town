<?php

function CSS($arquivo){
	$dir = 'css/';
	$sufixo = '.min';

	if(is_array($arquivo)){
		foreach($arquivo as $arq){
			if(is_array($arq)){
				foreach($arquivo as $arq2){
					if(file_exists($dir.$arq2.$sufixo.'.css'))
						echo "<link rel='stylesheet' href='".$dir.$arq2.$sufixo.".css'>";
					elseif(file_exists($dir.$arq2.'.css'))
						echo "<link rel='stylesheet' href='".$dir.$arq2.".css'>";
					elseif(file_exists($dir.$arq2.'.less'))
						echo "<link rel='stylesheet/less' type='text/css' href='".$dir.$arq2.".less?".time()."'>";
					else
						log_message('info', 'Erro ao carregar arquivo CSS ('.$arq2.')');
				}
			}else{
				if(file_exists($dir.$arq.$sufixo.'.css'))
					echo "<link rel='stylesheet' href='".$dir.$arq.$sufixo.".css'>";
				elseif(file_exists($dir.$arq.'.css'))
					echo "<link rel='stylesheet' href='".$dir.$arq.".css'>";
				elseif(file_exists($dir.$arq.'.less'))
					echo "<link rel='stylesheet/less' type='text/css' href='".$dir.$arq.".less?".time()."'>";
				else
					log_message('info', 'Erro ao carregar arquivo CSS ('.$arq.')');
			}
		}
	}else{
		if(file_exists($dir.$arquivo.$sufixo.'.css'))
			echo "<link rel='stylesheet' href='".$dir.$arquivo.$sufixo.".css'>";
		elseif(file_exists($dir.$arquivo.'.css'))
				echo "<link rel='stylesheet' href='".$dir.$arquivo.".css'>";
		elseif(file_exists($dir.$arquivo.'.less'))
			echo "<link rel='stylesheet/less' type='text/css' href='".$dir.$arquivo.".less?".time()."'>";
		else
			log_message('info', 'Erro ao carregar arquivo CSS ('.$arquivo.')');
	}
}

function JS($arquivo){
	$dir = 'js/';
	$extensao = '.js';
	$extensao_nocache = '.js?'.time();

	if(ENVIRONMENT != 'development')
		$extensao_nocache = $extensao;
	
	if(is_array($arquivo)){

		foreach($arquivo as $arq){

			if(is_array($arq)){

				foreach($arquivo as $arq2){

					if(file_exists($dir.$arq2.$extensao))
						echo "<script src='".$dir.$arq2.$extensao_nocache."' type='text/javascript'></script>";
					else
						log_message('info', 'Erro ao carregar arquivo JS ('.$arq2.')');

				}

			}else{

				if(file_exists($dir.$arq.$extensao))
					echo "<script src='".$dir.$arq.$extensao_nocache."' type='text/javascript'></script>";
				else
					log_message('info', 'Erro ao carregar arquivo JS ('.$arq.')');

			}
		}

	}else{
		if(file_exists($dir.$arquivo.$extensao))
			echo "<script src='".$dir.$arquivo.$extensao_nocache."' type='text/javascript'></script>";
		else
			log_message('info', 'Erro ao carregar arquivo JS ('.$arquivo.')');
	}
}

?>