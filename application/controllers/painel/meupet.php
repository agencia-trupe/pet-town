<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Meupet extends MY_Admincontroller {

    function __construct(){
	   	parent::__construct();

	   	$this->load->model('meupet_model', 'model');
    }

    function aprovar($id = false){
    	
    	if($id){

    		$this->db->set('publicar', 1)
    				 ->where('id', $id)
    				 ->update('meupet');

    		$this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Publicado com sucesso');
		}else{
			$this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao Publicar');
		}

		redirect('painel/meupet', 'refresh');
    }
}