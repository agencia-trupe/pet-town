<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Delivery extends MY_Admincontroller {

    function __construct(){
	   	parent::__construct();

	   	$this->load->model('delivery_model', 'model');
    }

    function index_bairros(){
    	$data['registros'] = $this->db->order_by('titulo', 'asc')->get('delivery_bairros')->result();
    	$this->load->view('painel/delivery/bairros', $data);
    }

    function inserirBairro(){

    	$insert = $this->db->set('titulo', $this->input->post('titulo'))->insert('delivery_bairros');

        if($insert){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Bairro inserido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir bairro');
        }

        redirect('painel/'.$this->router->class.'/index_bairros', 'refresh');
    }

    function excluirBairro($id = false){

    	if($id)
    		$delete = $this->db->where('id', $id)->delete('delivery_bairros');
    	else
    		$delete = false;

        if($delete){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' excluido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index_bairros', 'refresh');    	
    }

}