<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos extends MY_Admincontroller {

    function __construct(){
   		parent::__construct();

   		$this->load->model('produtos_model', 'model');
    }

    function busca($filtro_secao = 'todos', $filtro_categoria = 'todos', $filtro_animais = 'todos'){

        $termo = $this->input->post('termo');

        if(!$termo)
            redirect('painel/produtos');

        $data['registros'] = $this->model->busca($termo);
        $data['titulo'] = 'Busca de Produtos - '.$termo;
        
        foreach ($data['registros'] as $key => $value) {
            $value->imagens = $this->model->pegarVariacoes($value->id);
        }

        $data['filtro_secao'] = $filtro_secao;
        $data['filtro_categoria'] = $filtro_categoria;
        $data['filtro_animais'] = $filtro_animais;
        
        $data['secoes'] = $this->model->pegarSecoes();
        $data['animais'] = $this->model->pegarAnimais();
        $data['categorias'] = $this->model->pegarCategorias();
        
        $this->load->view('painel/'.$this->router->class.'/lista', $data);        
    }

    function index($filtro_secao = 'todos', $filtro_categoria = 'todos', $filtro_animais = 'todos'){
        if($filtro_secao != 'todos' || $filtro_categoria != 'todos' || $filtro_animais != 'todos'){
            $data['titulo'] = 'Produtos - Seção : '.$this->model->pegaTitulo('secoes', $filtro_secao).' > Categoria : '.$this->model->pegaTitulo('categorias', $filtro_categoria).' > Animais : '.$this->model->pegaTitulo('animais', $filtro_animais);
            $data['registros'] = $this->model->pegarComFiltros($filtro_secao, $filtro_categoria, $filtro_animais);
        }else{
            $data['registros'] = $this->model->pegarTodos();
            $data['titulo'] = 'Todos os Produtos';
        }

        foreach ($data['registros'] as $key => $value) {
            $value->imagens = $this->model->pegarVariacoes($value->id);
        }

        $data['filtro_secao'] = $filtro_secao;
        $data['filtro_categoria'] = $filtro_categoria;
        $data['filtro_animais'] = $filtro_animais;
        
        $data['secoes'] = $this->model->pegarSecoes();
        $data['animais'] = $this->model->pegarAnimais();

        if($filtro_secao != 'todos')
            $data['categorias'] = $this->model->pegarCategoriasPorSecao($this->model->idSecao($filtro_secao));
        else
            $data['categorias'] = $this->model->pegarCategorias();

        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
            $data['arr_categorias'] = $this->model->categoriasDoProduto($id);
            $data['arr_animais'] = $this->model->animaisDoProduto($id);
        }else{
            $data['registro'] = FALSE;
        }

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $data['categorias'] = $this->model->pegarCategorias();
        $data['animais'] = $this->model->pegarAnimais();
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }

    function imagens($id_produtos, $id_imagem =  FALSE){
        if($id_imagem){
            $data['parent'] = $this->model->pegarPorId($id_produtos);
            $data['imagens'] = $this->model->imagens($id_produtos);
            $data['registro'] = $this->model->imagens($id_produtos, $id_imagem);
            if(!$data['parent'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['parent'] = $this->model->pegarPorId($id_produtos);
            $data['imagens'] = $this->model->imagens($id_produtos);
            $data['registro'] = FALSE;
        }
        
        $data['campo_1'] = "Imagem";
        $data['titulo'] = 'Galeria de imagens de : '.$data['parent']->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/imagens', $data);
    }

    function inserirImagem(){
        if($this->model->inserirImagem()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem inserida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$this->input->post('id_parent'), 'refresh');
    }

    function editarImagem($id_imagem){
        if($this->model->editarImagem($id_imagem)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem alterada com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$this->input->post('id_parent'), 'refresh');
    }

    function excluirImagem($id_imagem, $id_album){
        if($this->model->excluirImagem($id_imagem)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem excluida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens/'.$id_album, 'refresh');
    }    

    function categorias($secao = false){
        $data['registros'] = $this->model->pegarCategoriasPorSecao($secao);
        $data['secoes'] = $this->model->pegarSecoes();
        $data['marcar_secao'] = $secao;

        $data['titulo'] = 'Categorias';
        $data['unidade'] = 'Categoria';
        $this->load->view('painel/'.$this->router->class.'/lista_categorias', $data);
    }

    function form_categorias($id = false){
        if($id){
            $data['registro'] = $this->model->pegarCategorias($id);
            $data['titulo'] = 'Alterar Categorias';
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
        }else{
        	$data['titulo'] = 'Inserir Categorias';
            $data['registro'] = FALSE;
        }

        $data['secoes'] = $this->model->pegarSecoes();
        $data['unidade'] = 'Categoria';
        $this->load->view('painel/'.$this->router->class.'/form_categorias', $data);    	
    }

    function adicionar_categorias(){
        if($this->model->inserir_categoria()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria inserida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir Categoria');
        }

        redirect('painel/'.$this->router->class.'/categorias', 'refresh');
    }

    function alterar_categorias($id){
        if($this->model->alterar_categoria($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria alterada com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Categoria');
        }

        redirect('painel/'.$this->router->class.'/categorias', 'refresh');
    }

    function excluir_categorias($id){
        if($this->model->excluir_categoria($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria excluido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Categoria');
        }

        redirect('painel/'.$this->router->class.'/categorias', 'refresh');
    }

}