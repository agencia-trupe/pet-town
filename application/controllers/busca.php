<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Busca extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$this->load->model('produtos_model', 'model');

    	$termo = $this->input->post('termo');

    	$data['resultados_boutique'] = $this->model->busca($termo, 'boutique');
    	$data['resultados_alimentacao'] = $this->model->busca($termo, 'alimentacao');
    	$data['resultados_higiene'] = $this->model->busca($termo, 'higiene_e_beleza');
    	$data['resultados_farmacia'] = $this->model->busca($termo, 'farmacia');
    	$data['termo'] = $termo;

    	foreach ($data['resultados_boutique'] as $key => $value) {
			$value->imagens = $this->model->pegarVariacoes($value->id);
	        $value->imagem = isset($value->imagens[0]) ? $value->imagens[0]->imagem : false;
	        $value->link = $this->model->gerarLink($value->id, 'todos', 'todos', TRUE);
	        $value->filtro_secao = 'boutique';
	        $value->titulo_secao = 'Boutique';
    	}

    	foreach ($data['resultados_alimentacao'] as $key => $value) {
			$value->imagens = $this->model->pegarVariacoes($value->id);
	        $value->imagem = isset($value->imagens[0]) ? $value->imagens[0]->imagem : false;
	        $value->link = $this->model->gerarLink($value->id, 'todos', 'todos', TRUE);
	        $value->filtro_secao = 'alimentacao';
	        $value->titulo_secao = 'Alimentação';
    	}

    	foreach ($data['resultados_higiene'] as $key => $value) {
			$value->imagens = $this->model->pegarVariacoes($value->id);
	        $value->imagem = isset($value->imagens[0]) ? $value->imagens[0]->imagem : false;
	        $value->link = $this->model->gerarLink($value->id, 'todos', 'todos', TRUE);
	        $value->filtro_secao = 'higiene_e_beleza';
	        $value->titulo_secao = 'Higiene & Beleza';
    	}

    	foreach ($data['resultados_farmacia'] as $key => $value) {
			$value->imagens = $this->model->pegarVariacoes($value->id);
	        $value->imagem = isset($value->imagens[0]) ? $value->imagens[0]->imagem : false;
	        $value->link = $this->model->gerarLink($value->id, 'todos', 'todos', TRUE);
	        $value->filtro_secao = 'farmacia';
	        $value->titulo_secao = 'Farmácia';
    	}

      	$this->load->view('busca', $data);    	
    }

}