<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresa extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct();

   		$this->headervar['title'] = "Pet Town - Empresa";
   }

   function index(){
      $this->load->model('empresa_model', 'empresa');

      $data['registro'] = $this->empresa->pegarTodos();

      $this->load->view('empresa', $data);
   }

}