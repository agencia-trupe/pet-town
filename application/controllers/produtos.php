<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct();
   }

   function index($secao_slug = 'todos', $categoria_slug = 'todos', $animais_slug = 'todos', $pagina = 0){
      $this->load->model('produtos_model', 'model');
      $this->load->library('pagination');

      $pag_options = array(
         'base_url' => base_url("produtos/index/$secao_slug/$categoria_slug/$animais_slug/"),
         'per_page' => 25,
         'uri_segment' => 6,
         'next_link' => "<span class='pad-l'>mais produtos &raquo;</span>",
         'prev_link' => "<span class='pad-r'>&laquo; produtos anteriores</span>",
         'display_pages' => FALSE,
         'first_link' => FALSE,
         'last_link' => FALSE
      );

      if ($animais_slug != 'todos') {
         $this->session->set_flashdata('animais', $animais_slug);
      }

      if($secao_slug != 'todos' || $categoria_slug != 'todos' || $animais_slug != 'todos'){
         $pag_options['total_rows'] = $this->model->pegarComFiltros($secao_slug, $categoria_slug, $animais_slug, TRUE);
         $data['registros'] = $this->model->pegarComFiltros($secao_slug, $categoria_slug, $animais_slug, FALSE, $pag_options['per_page'], $pagina);
      }else{
         $pag_options['total_rows'] = $this->model->pegarTodos(TRUE);
         $data['registros'] = $this->model->pegarTodos(FALSE, $pag_options['per_page'], $pagina);
      }

      $this->pagination->initialize($pag_options);
      $data['paginacao'] = $this->pagination->create_links();

      foreach ($data['registros'] as $key => $value) {
         $value->imagens = $this->model->pegarVariacoes($value->id);
         $value->imagem = isset($value->imagens[0]) ? $value->imagens[0]->imagem : false;
         $value->link = $this->model->gerarLink($value->id, $categoria_slug, $animais_slug);
      }

      $data['filtro_secao'] = $secao_slug;
      $data['filtro_categoria'] = $categoria_slug;
      $data['filtro_animais'] = $animais_slug;
      $data['titulo_secao'] = $this->model->pegaTitulo('secoes', $secao_slug);
      $data['breadcrumbs'] = $this->model->geraBreadcrumb($secao_slug, $categoria_slug, $animais_slug);
     
      $data['secoes'] = $this->model->pegarSecoes();
      $data['animais'] = $this->model->pegarAnimais();

      if($secao_slug != 'todos')
         $data['categorias'] = $this->model->pegarCategoriasPorSecao($this->model->idSecao($secao_slug));
      else
         $data['categorias'] = $this->model->pegarCategorias();      

      $this->headervar['title'] = "Pet Town - Produtos";

      $this->load->view('produtos/lista', $data);
   }

   function detalhes($secao_slug = 'todos', $categoria_slug = 'todos', $animais_slug = 'todos', $produtos_slug = false){
      $this->load->model('produtos_model', 'model');

      if ($animais_slug != 'todos') {
         $this->session->set_flashdata('animais', $animais_slug);
      }

      if($produtos_slug){

         $data['detalhe'] = $this->model->pegarPorSlug($produtos_slug);

         if(!$data['detalhe'])
            redirect('produtos/index/'.$secao_slug.'/'.$categoria_slug.'/'.$animais_slug);

         $data['detalhe']->imagens = $this->model->pegarVariacoes($data['detalhe']->id);
         $data['detalhe']->imagem = isset($data['detalhe']->imagens[0]) ? $data['detalhe']->imagens[0]->imagem : false;
         $data['detalhe']->legenda = isset($data['detalhe']->imagens[0]) ? $data['detalhe']->imagens[0]->legenda : '';
         $data['detalhe']->link = $this->model->gerarLink($data['detalhe']->id, $categoria_slug);
         $data['detalhe']->animais = $this->model->animaisDoProduto($data['detalhe']->id, TRUE);
      }else{
         redirect('produtos/index/$secao_slug/$categoria_slug/$animais_slug');
      }

      $data['registros'] = $this->model->pegarRelacionados($secao_slug, $categoria_slug, $animais_slug, $data['detalhe']->id);

      foreach ($data['registros'] as $key => $value) {
         $value->imagens = $this->model->pegarVariacoes($value->id);
         $value->imagem = isset($value->imagens[0]) ? $value->imagens[0]->imagem : false;
         $value->link = $this->model->gerarLink($value->id, $categoria_slug, $animais_slug);
      }      
      
      $data['filtro_secao'] = $secao_slug;
      $data['filtro_categoria'] = $categoria_slug;
      $data['filtro_animais'] = $animais_slug;
      $data['titulo_secao'] = $this->model->pegaTitulo('secoes', $secao_slug);
      $data['breadcrumbs'] = $this->model->geraBreadcrumb($secao_slug, $categoria_slug, $animais_slug);
     
      $data['secoes'] = $this->model->pegarSecoes();
      $data['animais'] = $this->model->pegarAnimais();

      if($secao_slug != 'todos')
         $data['categorias'] = $this->model->pegarCategoriasPorSecao($this->model->idSecao($secao_slug));
      else
         $data['categorias'] = $this->model->pegarCategorias();

      $this->headervar['title'] = $data['detalhe']->titulo;
      $this->headervar['descricao'] = $data['detalhe']->descricao;
      $this->headervar['imagem'] = ($data['detalhe']->imagem) ? $data['detalhe']->imagem : false;

      if ($produtos_slug) {
         $this->load->view('produtos/detalhe', $data);
      } else {
         $this->load->view('produtos/lista', $data);   
      }
      
   }

   function modal($secao_slug = 'todos', $categoria_slug = 'todos', $animais_slug = 'todos', $produtos_slug = false){
      $this->load->model('produtos_model', 'model');

      if ($animais_slug != 'todos') {
         $this->session->set_flashdata('animais', $animais_slug);
      }

      if($produtos_slug){

         $data['detalhe'] = $this->model->pegarPorSlug($produtos_slug);

         if(!$data['detalhe'])
            redirect('produtos/index/'.$secao_slug.'/'.$categoria_slug.'/'.$animais_slug);

         $data['detalhe']->imagens = $this->model->pegarVariacoes($data['detalhe']->id);
         $data['detalhe']->imagem = isset($data['detalhe']->imagens[0]) ? $data['detalhe']->imagens[0]->imagem : false;
         $data['detalhe']->legenda = isset($data['detalhe']->imagens[0]) ? $data['detalhe']->imagens[0]->legenda : '';
         $data['detalhe']->link = $this->model->gerarLink($data['detalhe']->id, $categoria_slug);
         $data['detalhe']->animais = $this->model->animaisDoProduto($data['detalhe']->id, TRUE);
         $data['detalhe']->imagem_facebook = isset($data['detalhe']->imagens[0]) ? base_url('_imgs/produtos/'.$data['detalhe']->imagens[0]) : base_url('_imgs/layout/img_facebook.png');
      }else{
         redirect('produtos/index/$secao_slug/$categoria_slug/$animais_slug');
      }

      $data['registros'] = $this->model->pegarRelacionados($secao_slug, $categoria_slug, $animais_slug, $data['detalhe']->id);

      foreach ($data['registros'] as $key => $value) {
         $value->imagens = $this->model->pegarVariacoes($value->id);
         $value->imagem = isset($value->imagens[0]) ? $value->imagens[0]->imagem : false;
         $value->link = $this->model->gerarLink($value->id, $categoria_slug, $animais_slug);
      }      
      
      $data['filtro_secao'] = $secao_slug;
      $data['filtro_categoria'] = $categoria_slug;
      $data['titulo_categoria'] = $this->model->pegaTitulo('categorias', $categoria_slug);
      $data['filtro_animais'] = $animais_slug;
      $data['titulo_secao'] = $this->model->pegaTitulo('secoes', $secao_slug);
      $data['breadcrumbs'] = $this->model->geraBreadcrumb($secao_slug, $categoria_slug, $animais_slug);
     
      $data['secoes'] = $this->model->pegarSecoes();
      $data['animais'] = $this->model->pegarAnimais();

      if($secao_slug != 'todos')
         $data['categorias'] = $this->model->pegarCategoriasPorSecao($this->model->idSecao($secao_slug));
      else
         $data['categorias'] = $this->model->pegarCategorias();      

      $this->hasLayout = FALSE;

      if ($produtos_slug)
         $this->load->view('produtos/modal', $data);

   }

}