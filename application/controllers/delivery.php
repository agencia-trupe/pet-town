<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Delivery extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct();

   		$this->headervar['title'] = "Pet Town - Delivery";
   }

   function index(){
      $this->load->model('delivery_model', 'delivery');

      $data['registro'] = $this->delivery->pegarTodos();

      $data['bairros'] = $this->delivery->pegarBairros();

      $this->load->view('delivery', $data);
   }

}