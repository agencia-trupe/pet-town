<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct();
   }

   function index(){
      $this->load->model('produtos_model', 'produtos');

		$data['amostra_galeria'] = $this->db->order_by('rand()')->get_where('meupet', array('publicar' => 1), 3)->result();

      $amostras = array(
         0 => array('boutique' => $this->produtos->pegarPorSecao('slug', 'boutique', 20)),
         1 => array('alimentacao' => $this->produtos->pegarPorSecao('slug', 'alimentacao', 20)),
         2 => array('higiene' => $this->produtos->pegarPorSecao('slug', 'higiene_e_beleza', 20)),
         3 => array('farmacia' => $this->produtos->pegarPorSecao('slug', 'farmacia', 20))
      );

      $indices = array_rand($amostras, 3);

      $data['amostra_produtos'] = array(
         $amostras[$indices[0]],
         $amostras[$indices[1]],
         $amostras[$indices[2]]
      );

      $this->load->view('home', $data);
   }

}