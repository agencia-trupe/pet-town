<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Marcas extends MY_Frontcontroller {

   function __construct(){
   		parent::__construct();

   		$this->headervar['title'] = "Pet Town - Marcas";
   }

   function index(){
      
   	$data['marcas'] = $this->db->order_by('titulo', 'asc')->get('marcas')->result();

      $this->load->view('marcas', $data);
   }

}