<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends MY_Frontcontroller {

   	function __construct(){
   		parent::__construct();

        $this->headervar['title'] = "Pet Town - Contato";
   	}

   	function index(){
      
   		$this->load->model('contato_model', 'model');

   		$data['contato'] = $this->model->pegarTodos();
   	
    	$this->load->view('contato', $data);
   	}

   	function fale(){

   		$nome = $this->input->post('nome');
   		$email = $this->input->post('email');
   		$telefone = $this->input->post('telefone');
   		$mensagem = $this->input->post('mensagem');

        if($nome == ''){
            $this->session->set_flashdata('mensagem_erro', 'Informe seu nome!');
            $this->session->set_flashdata('campo', "contato-nome");
            $this->session->set_flashdata('contato-nome', $nome);
            $this->session->set_flashdata('contato-email', $email);
            $this->session->set_flashdata('contato-telefone', $telefone);
            $this->session->set_flashdata('contato-mensagem', $mensagem);
            redirect('contato', 'refresh');
        }  
        
        if($email == '' || !preg_match("/\S+@\S+\.\S+/", $email)){
            $this->session->set_flashdata('mensagem_erro', 'Informe um email válido!');
            $this->session->set_flashdata('campo', "contato-email");
            $this->session->set_flashdata('contato-nome', $nome);
            $this->session->set_flashdata('contato-email', $email);
            $this->session->set_flashdata('contato-telefone', $telefone);
            $this->session->set_flashdata('contato-mensagem', $mensagem);
            redirect('contato', 'refresh');
        } 

        if($mensagem == ''){
            $this->session->set_flashdata('mensagem_erro', 'Informe sua mensagem!');
            $this->session->set_flashdata('campo', "contato-mensagem");
            $this->session->set_flashdata('contato-nome', $nome);
            $this->session->set_flashdata('contato-email', $email);
            $this->session->set_flashdata('contato-telefone', $telefone);
            $this->session->set_flashdata('contato-mensagem', $mensagem);
            redirect('contato', 'refresh');
        }

        $this->enviaEmail('contato');

        $this->session->set_flashdata('mensagem_sucesso_contato', true);
        redirect('contato', 'refresh');
   	}

	function trabalhe(){
   		$nome = $this->input->post('nome');
   		$email = $this->input->post('email');
   		$telefone = $this->input->post('telefone');
   		$mensagem = $this->input->post('mensagem');
   		$arquivo = $this->sobeArquivo();

        if($nome == ''){
            $this->session->set_flashdata('mensagem_erro', 'Informe seu nome!');
            $this->session->set_flashdata('campo', "trabalhe-nome");
            $this->session->set_flashdata('abrir_form_trabalhe', TRUE);
            $this->session->set_flashdata('trabalhe-nome', $nome);
            $this->session->set_flashdata('trabalhe-email', $email);
            $this->session->set_flashdata('trabalhe-telefone', $telefone);
            $this->session->set_flashdata('trabalhe-mensagem', $mensagem);
            redirect('contato', 'refresh');
        }  
        
        if($email == '' || !preg_match("/\S+@\S+\.\S+/", $email)){
            $this->session->set_flashdata('mensagem_erro', 'Informe um email válido!');
            $this->session->set_flashdata('abrir_form_trabalhe', TRUE);
            $this->session->set_flashdata('campo', "trabalhe-email");
            $this->session->set_flashdata('trabalhe-nome', $nome);
            $this->session->set_flashdata('trabalhe-email', $email);
            $this->session->set_flashdata('trabalhe-telefone', $telefone);
            $this->session->set_flashdata('trabalhe-mensagem', $mensagem);
            redirect('contato', 'refresh');
        } 

        if($mensagem == ''){
            $this->session->set_flashdata('mensagem_erro', 'Informe sua mensagem!');
            $this->session->set_flashdata('abrir_form_trabalhe', TRUE);
            $this->session->set_flashdata('campo', "trabalhe-mensagem");
            $this->session->set_flashdata('trabalhe-nome', $nome);
            $this->session->set_flashdata('trabalhe-email', $email);
            $this->session->set_flashdata('trabalhe-telefone', $telefone);
            $this->session->set_flashdata('trabalhe-mensagem', $mensagem);
            redirect('contato', 'refresh');
        }

        if(!$arquivo){
        	$this->session->set_flashdata('mensagem_erro', 'Envie seu currículo! (.doc, .docx ou .pdf)');
        	$this->session->set_flashdata('abrir_form_trabalhe', TRUE);
            $this->session->set_flashdata('campo', "file-input");
            $this->session->set_flashdata('trabalhe-nome', $nome);
            $this->session->set_flashdata('trabalhe-email', $email);
            $this->session->set_flashdata('trabalhe-telefone', $telefone);
            $this->session->set_flashdata('trabalhe-mensagem', $mensagem);
            redirect('contato', 'refresh');
        }

        $this->enviaEmail('trabalhe', $arquivo);

        $this->session->set_flashdata('mensagem_sucesso_trabalhe', true);
        redirect('contato', 'refresh');
	}

    /*
    Contato - pet7662
    noreply
    */

	private function enviaEmail($tipo, $arquivo = false){
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';

        $this->load->library('email');

        $this->email->initialize($emailconf);

        if ($tipo == 'contato') {
        	$to = 'contato@pettown.com.br';
        	$assunto = 'Contato via Site';
        	$path_arquivo = FALSE;
        } else {
        	$to = 'contato@pettown.com.br';
        	$assunto = 'Contato via Site - Trabalhe Conosco';
        	$path_arquivo = "_temp/".$arquivo;
        }

        $from = 'noreply@pettown.com.br';
        $fromname = 'Pet Town';
        $bcc = 'bruno@trupe.net';
        
        $u_nome = $this->input->post('nome');
        $u_email = $this->input->post('email');
        $u_telefone = $this->input->post('telefone');
        $u_mensagem = $this->input->post('mensagem');
                
        $email = "<html>
                    <head>
                        <style type='text/css'>
                            .tit{
                                font-weight:bold;
                                font-size:16px;
                                color:#00B69C;
                                font-family:Verdana;
                            }
                            .val{
                                color:#000;
                                font-size:14px;
                                font-family:Verdana;
                            }
                        </style>
                    </head>
                    <body>
                    <span class='tit'>Nome :</span> <span class='val'>$u_nome</span><br />
                    <span class='tit'>E-mail :</span> <span class='val'>$u_email</span><br />
                    <span class='tit'>Telefone :</span> <span class='val'>$u_telefone</span><br />
                    <span class='tit'>Mensagem :</span> <span class='val'>$u_mensagem</span>
                    </body>
                    </html>";

        $this->email->from($from, $fromname);
        $this->email->to($to);
        if($bcc)
            $this->email->bcc($bcc);
        $this->email->reply_to($u_email);

        if($path_arquivo)
        	$this->email->attach($path_arquivo);

        $this->email->subject($assunto);
        $this->email->message($email);

        $this->email->send();

        if($path_arquivo)
        	@unlink($path_arquivo);
	}

	private function sobeArquivo(){
		$this->load->library('upload');

		$original = array(
			'campo' => 'userfile',
			'dir' => '_temp/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'doc|docx|pdf|txt',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	//die($this->upload->display_errors());
		    	return false;
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
		        return $filename;
		    }
		}else{
		    return false;
		}
	}

}