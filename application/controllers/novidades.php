<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Novidades extends MY_Frontcontroller {

   function __construct(){
   	parent::__construct();

      $this->load->model('novidades_model', 'novidades');
      $this->headervar['title'] = "Pet Town - Novidades";
   }

   function index($pag = 0){

      $this->load->library('pagination');

      $config['base_url'] = base_url('novidades/index/');
      $config['per_page'] = 5;
      $config['uri_segment'] = 3;
      $config['next_link'] = "<span class='pad-l'>notícias mais antigas &raquo;</span>";
      $config['prev_link'] = "<span class='pad-r'>&laquo; notícias mais recentes</span>";
      $config['display_pages'] = FALSE;
      $config['first_link'] = FALSE;
      $config['last_link'] = FALSE;
      $config['total_rows'] = $this->db->get('novidades')->num_rows();

      $this->pagination->initialize($config); 
      $data['paginacao'] = $this->pagination->create_links();    

      $data['novidades'] = $this->novidades->pegarPaginado($config['per_page'], $pag, 'data', 'DESC');

      $data['pagina_atual'] = $pag;

      $this->load->view('novidades/lista', $data);
   }

   function detalhes($pagina_atual = 0, $slug_novidades = false){

      if(!$slug_novidades)
         redirect('novidades');

      $qry = $this->db->get_where('novidades', array('slug' => $slug_novidades))->result();

      if(!isset($qry[0]))
         redirect('novidades');

      $data['registro'] = $qry[0];
      //Mais recente
      $data['proximo'] = $this->novidades->linkProximo($qry[0]->data);
      //Mais antigo
      $data['anterior'] = $this->novidades->linkAnterior($qry[0]->data);
      $data['voltar_pagina'] = $pagina_atual;
      $this->load->view('novidades/detalhe', $data);
   }


}