<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galeria extends MY_Frontcontroller {

    function __construct(){
        parent::__construct();

        $this->headervar['title'] = "Pet Town - Galeria de Pets";
    }

    function index(){
        $this->load->model('meupet_model', 'pets');

        $data['total_resultados'] = $this->db->get_where('meupet', array('publicar' => 1))->num_rows();
        $data['porpagina'] = 10;

        $data['registros'] = $this->pets->pegarAprovados($data['porpagina']);

        $this->load->view('meupet/galeria', $data);
    }

    function enviar(){
        $this->load->view('meupet/form');
    }

    function submeterAprovacao(){

        $this->load->model('meupet_model', 'pets');

        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $idade = $this->input->post('idade');
        $nome_pet = $this->input->post('pet_nome');
        $legenda = $this->input->post('pet_legenda');
        $imagem = $this->pets->sobeImagem('pet_imagem');

        $this->session->set_flashdata('flash_nome', $nome);
        $this->session->set_flashdata('flash_email', $email);
        $this->session->set_flashdata('flash_idade', $idade);
        $this->session->set_flashdata('flash_nome_pet', $nome_pet);
        $this->session->set_flashdata('flash_legenda', $legenda);
        
        if($nome == ''){
            $this->session->set_flashdata('mensagem_erro', 'Informe seu nome!');
            $this->session->set_flashdata('campo', "input-nome");
            redirect('galeria/enviar');
        }
        
        if($email == '' || !preg_match("/\S+@\S+\.\S+/", $email)){
            $this->session->set_flashdata('mensagem_erro', 'Informe um email válido!');
            $this->session->set_flashdata('campo', "input-email");
            redirect('galeria/enviar');
        }

        if($idade == ''){
            $this->session->set_flashdata('mensagem_erro', 'Informe sua idade!');
            $this->session->set_flashdata('campo', "input-idade");
            redirect('galeria/enviar');   
        }

        if($nome_pet == ''){
            $this->session->set_flashdata('mensagem_erro', 'Informe o nome de seu Animal de Estimação!');
            $this->session->set_flashdata('campo', "input-pet_nome");
            redirect('galeria/enviar');
        }

        if($legenda == ''){
            $this->session->set_flashdata('mensagem_erro', 'Informe a legenda da Imagem!');
            $this->session->set_flashdata('campo', "input-pet_legenda");
            redirect('galeria/enviar');
        }

        if(!$imagem){
            $this->session->set_flashdata('mensagem_erro', 'Selecione a imagem de seu Animal de Estimação!');
            $this->session->set_flashdata('campo', "input-pet_imagem");
            redirect('galeria/enviar');
        }

        $insert = $this->db->set('nome_dono', $nome)
                           ->set('email_dono', $email)
                           ->set('idade_dono', $idade)
                           ->set('nome', $nome_pet)
                           ->set('legenda', $legenda)
                           ->set('imagem', $imagem)
                           ->set('data_envio', Date('Y-m-d H:i:s'))
                           ->insert('meupet');

        // Redirecionar para a galeria com mensagem de agradecimento + moderação
        $this->session->set_flashdata('mensagem_sucesso', true);
        redirect('galeria');
    }



}