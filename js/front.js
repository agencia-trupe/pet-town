/* Definição do método .map() para browsers que não suportam  */
if(!Array.prototype.map){
	Array.prototype.map = function(callback, thisArg) {  
	  var T, A, k; 

	  if (this == null) {  
	    throw new TypeError(" this is null or not defined");  
	  }  

	  var O = Object(this);  

	  var len = O.length >>> 0;  

	  if ({}.toString.call(callback) != "[object Function]") {  
	    throw new TypeError(callback + " is not a function");  
	  }  

	  if (thisArg) {  
	    T = thisArg;  
	  }  

	  A = new Array(len);  
	  k = 0;  

	  while(k < len) {  

	    var kValue, mappedValue;  

	    if (k in O) {  

	      kValue = O[ k ];  

	      mappedValue = callback.call(T, kValue, k, O);  

	      A[ k ] = mappedValue;  
	    }  

	    k++;  
	  }

	  return A;  
	};
}

var alerta = function(elemento, mensagem){

	var caixa = $("<div class='alerta hid'>"+mensagem+"</div>");
	var campo = elemento;

	campo.before(caixa);
	
	caixa.removeClass('hid');
	campo.focus();
	$('html, body').animate({ 'scrollTop' : caixa.offset().top }, 0);
	setTimeout( function(){
		caixa.addClass('hid');
		setTimeout( function(){
			caixa.remove();
		}, 300);
	}, 20000);	

}

$('document').ready( function(){


	Modernizr.load([
		{
			test: Modernizr.input.placeholder,
		  	nope: 'js/polyfill-placeholder.js'
		}
	]);

	if (!Modernizr.csstransforms3d)
		$('.container-marca .box .lado.tras').hide();
	
	if (!Modernizr.opacity){
		$('.overlay').hide();
		$('.overlay-texto').hide();
		$('.portrait .legenda').remove();
		$('form .fundo-colorido .area-click').unbind('click');
		$('#form-trabalhe .hid').removeClass('hid');
	}
	
	$('#main-nav li.abre-submenu').hover( function(){

		var submenu = $(this).find('.submenu');
		submenu.removeClass('hid');

	}, function(){

		var submenu = $(this).find('.submenu');
		setTimeout( function(){
			submenu.addClass('hid');
		}, 200);

	});

	$('.fancy').fancybox();

	$('#animate').cycle({
		pager:  '#navegacao',
		pause : 1,
		timeout : 6000,
	    pagerAnchorBuilder: function(idx, slide) { 
	        return '<a href="#"></a>';
	    } 
	});

	$('.variacoes').click( function(e){
		e.preventDefault();

		var imagem  = $(this).attr('href');
		var alvo    = $('#imagem-ampliada');
		var alvo_legenda    = $('#legenda-imagem');
		var legenda = $(this).attr('title');

		alvo.prepend($("<img src='"+imagem+"'>"));
		setTimeout( function(){
			var primeira = $('img:last', alvo);
			alvo_legenda.html(legenda);
			primeira.fadeOut('slow', function(){
				$(this).remove();
			});
		}, 200);
	});

	if($('.wide').length){

		var $container = $('.wide');

		$container.imagesLoaded(function(){
		  	$container.isotope({
			  	layoutMode : 'masonry',
			  	itemSelector : '.portrait',
			  	masonry: {
					columnWidth: 300,
				    gutterWidth: 30
				}
		  	});

		  	$('.portrait').each( function(){
		  		var imagem = $(this).find('img');
		  		var altura = parseInt(imagem.css('height'));
		  		var legenda = $(this).find('.legenda');
		  		var padding = altura * 0.4;
		  		imagem.attr('height', altura + 'px');
		  		legenda.css({
		  			'height' : (altura - padding) + 'px',
		  			'padding-top' : padding + 'px'
		  		});
		  	});
		});

		$('.wide .portrait .frente').each( function() { $(this).hoverdir(); } );

	}

	$('#pegar-mais-imagens a').click( function(e){
		e.preventDefault();

		if(!$(this).hasClass('em-atividade')){
			$(this).addClass('em-atividade');

			var porpagina = $('#por_pagina').val();
			var max       = $('#total_resultados').val();
			var ultimo    = $(this).attr('data-ultimo');
			var botao     = $(this);

			var div, ultimo_id, contador = 1;

			$('#loader').removeClass('hid');

			$.post(BASE+'ajax/pegarFotos', { ultimo : ultimo , porpagina : porpagina }, function(retorno){
				retorno = JSON.parse(retorno);
				
				retorno.map( function(self){
					div  += "<div class='portrait'>";
						div += "<div class='frente lado'>";
							div += "<img src='_imgs/galerias/"+self.imagem+"'>";
							div += "<div class='nome'>"+self.nome+"</div>";
							div += "<div class='legenda'>"+self.legenda+"</div>";
						div += "</div>";
					div += "</div>";
					contador++;
				});

				var $novasImgs = $(div);

    			$novasImgs.imagesLoaded(function(){
					$('.wide').isotope( 'insert', $novasImgs, function(){

						$('.portrait').each( function(){
					  		var imagem = $(this).find('img');
					  		var altura = parseInt(imagem.css('height'));
					  		var legenda = $(this).find('.legenda');
					  		var padding = altura * 0.4;
					  		imagem.attr('height', altura + 'px');
					  		legenda.css({
					  			'height' : (altura - padding) + 'px',
					  			'padding-top' : padding + 'px'
					  		});
					  	});

					  	$('.wide .portrait .frente').each( function() { $(this).hoverdir(); } );

						$('html, body').animate({
							scrollTop: $('.wide .portrait:last').offset().top
			     		}, 500);

						botao.attr('data-ultimo', parseInt(ultimo) + parseInt(porpagina));
						$('#loader').addClass('hid');

						if( (parseInt(ultimo) + parseInt(porpagina)) >= max ){
							botao.fadeOut();
						}

						botao.removeClass('em-atividade');
					});
				});
			});
		}
	});

	$('#input-pet_imagem').change( function(){
		var conteudo = $(this).val();
		if(conteudo != '')
			$('#fakeinput').html(conteudo.split('\\').pop());
		else
			$('#fakeinput').html('ENVIAR IMAGEM');
	});

	$('#trabalhe-curriculo').change( function(){
		var conteudo = $(this).val();
		if(conteudo != '')
			$('#fake-input').html(conteudo.split('\\').pop());
		else
			$('#fake-input').html('ANEXAR CURRÍCULO &raquo;');
	});	

	$('#pet_form').submit( function(e){
		
		var nome     = $('#input-nome');
		var email    = $('#input-email');
		var idade    = $('#input-idade');
		var nome_pet = $('#input-pet_nome');
		var legenda  = $('#input-pet_legenda');
		var imagem   = $('#input-pet_imagem');
		
		if(nome.val() == ''){
			alerta(nome, "Informe seu nome!");
			return false;
		}

		if(email.val() == ''){
			alerta(email, "Informe seu email!");
			return false;
		}

		if(idade.val() == ''){
			alerta(idade, "Informe sua Idade!");
			return false;
		}

		if(nome_pet.val() == ''){
			alerta(nome_pet, "Informe o nome do seu Bichinho!");
			return false;
		}

		if(legenda.val() == ''){
			alerta(legenda, "Informe a legenda de sua foto!");
			return false;
		}

		if(imagem.val() == ''){
			alerta(imagem, "Envie a imagem de seu bichinho!");
			return false;
		}		

	});

	$('#form-contato').submit( function(){

		var nome     = $('#contato-nome');
		var email    = $('#contato-email');
		var mensagem = $('#contato-mensagem');

		if(nome.val() == ''){
			alerta(nome, "Informe seu nome!");
			return false;
		}

		if(email.val() == ''){
			alerta(email, "Informe seu email!");
			return false;
		}
		
		if(mensagem.val() == ''){
			alerta(mensagem, "Informe sua mensagem!");
			return false;
		}		

	});

	$('#form-trabalhe').submit( function(){

		var nome      = $('#trabalhe-nome');
		var email     = $('#trabalhe-email');
		var curriculo = $('#trabalhe-curriculo');

		if(nome.val() == ''){
			alerta(nome, "Informe seu nome!");
			return false;
		}

		if(email.val() == ''){
			alerta(email, "Informe seu email!");
			return false;
		}

		if(curriculo.val() == ''){
			alerta($('#file-input'), "Anexe seu currículo!");
			return false;
		}	
		
	});

	$('.container-marca .box').bind('tapone', function(){
		$(this).toggleClass('flipped');
	});

	$('form .fundo-colorido .area-click').bind('click', function(){
		var botao       = $(this);
		var alvo_form   = $('#hid-form');
		var alvo_submit = $('#hid-submit');

		botao.toggleClass('aberto');
		alvo_form.toggleClass('hid');
		alvo_submit.toggleClass('hid');
		if($('#trabalhe-nome').is(':visible')){
			$('#trabalhe-nome').focus();
		}
	});

});