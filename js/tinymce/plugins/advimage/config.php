<?php

// As default, the uploaded image will be resized to this size
define('DEFAULT_IMG_WIDTH', 580);

// There will in addition to the resized image be created a thumbnail. This is the thumbnail size
define('DEFAULT_THUMB_WIDTH', 100);

// The quality of the resized image - 0 = lowest -> 100 = highest
define('IMAGE_QUALITY', 100);

// Define the current location of the config.php file
define('CONF_PATH','js/tinymce/plugins/advimage/config.php');

$thisFile = str_replace('\\', '/', __FILE__);
$docRoot = $_SERVER['DOCUMENT_ROOT'];
$webRoot  = str_replace(array($docRoot, CONF_PATH), '', $thisFile);
$srvRoot  = str_replace(CONF_PATH, '', $thisFile);

if(substr($webRoot, 0,1) == "/"){
  define('WEB_ROOT', $webRoot); 
}else{
  define('WEB_ROOT', '/'.$webRoot);
}

define('SRV_ROOT', $srvRoot);

// When managing the images - this in the path to the resized image
define('IMG_PATH', SRV_ROOT.'_imgs/banco/'); 

// When uploading the image - the original will be uploaded, then deleted from this dir
define('IMG_PATH_TEMP', SRV_ROOT.'_imgs/banco/tmp/');

// This is the actual path to the image in the article
define('IMG_PATH_LIVE', WEB_ROOT.'_imgs/banco/'); 

// Every directory will have a subfolder containing thumbnails. This is the subfolders name. 
define('THUMB_DIR', 'thumbs/');

// Information in the upload section
define('UPLOAD_INFO', 'Formatos suportados: .png, .jpg, .gif. Para melhor qualidade as imagens devem ter no mínimo 500px de largura e 2Mb.');
?>
