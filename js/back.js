$(document).ready( function(){

    $('.voltar').click( function(){ window.history.back(); });

    $('#datepicker').datepicker();

    $('#monthpicker').monthpicker();

    if($('.mostrarerro').length || $('.mostrarsucesso').length){
        console.log('erro')
        $('.mensagem').show('normal', function(){
            $(this).delay(4000).slideUp('normal');
        })
    }

    $('.delete').click( function(){
        if(!confirm('Deseja Excluir o Registro ?'))
            return false;
    });

    $('input[type="checkbox"]').change( function(){
        if($(this).is(':checked')){
            $(this).parent().css('font-weight', 'bold');
        }else{
            $(this).parent().css('font-weight', 'normal');
        }
    });

    $('input[type="checkbox"]:checked').each( function(){
        $(this).parent().css('font-weight', 'bold');
    });

    $('.submenu .nome-filtro').click( function(e){
        var alvo = $(this).parent().find('.filtros');
        var img = $(this).find('img');
        if(alvo.hasClass('hid')){
            alvo.removeClass('hid');
            img.attr('src', 'css/painel/arrow_up.png');
        }else{
            alvo.addClass('hid');
            img.attr('src', 'css/painel/arrow_down.png');
        }
    });

    $('.submenu .nome-filtro').click( function(e){
        var alvo = $(this).parent().find('.imgform');
        var img = $(this).find('img');
        if(alvo.hasClass('hid')){
            alvo.removeClass('hid');
            img.attr('src', 'css/painel/arrow_up.png');
        }else{
            alvo.addClass('hid');
            img.attr('src', 'css/painel/arrow_down.png');
        }
    });    

    $('.filtros').each( function(){
        var ativo = $(this).find('.active');
        if(ativo.length){
            if(ativo.html() != 'Todos'){
                $(this).removeClass('hid');
                $(this).parent().find('.nome-filtro img').attr('src', 'css/painel/arrow_up.png');
            }
        }
    });

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "comimagem",
        theme : "advanced",
        theme_advanced_buttons1 : "formatselect,separator,bold,italic,underline,separator,link,unlink,separator,jbimages",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_blockformats : "h1, h2, p",
        theme_advanced_toolbar_location : "top",
        plugins: "paste,jbimages",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
        width : 660,
        height : 600
    });

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "semimagem",
        theme : "advanced",
        theme_advanced_buttons1 : "formatselect,separator,bold,italic,underline,separator,link,unlink",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_blockformats : "h1, h2, p",
        theme_advanced_toolbar_location : "top",
        plugins: "paste",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        }      
    });

    tinyMCE.init({
        language : "pt",
        mode : "specific_textareas",
        editor_selector : "basico",
        theme : "advanced",
        theme_advanced_buttons1 : "bold,italic,underline,link,unlink",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        plugins: "paste",
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                ed.pasteAsPlainText = true;
            });
        },
        width:660,
        height:150
    });    
});